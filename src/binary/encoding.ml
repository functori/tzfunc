(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Reader = Reader
module Writer = Writer

let (let|) = Reader.(let|)

type error = [ `no_case_matched of int option | `recursive_error ]

type 'a direct_encoding = {
  ser : ('a -> (Crypto.Raw.t, [ Writer.error | error ]) result);
  de : (Reader.state -> ('a * Reader.state, [ Reader.error | error ]) result);
  size : ('a -> int);
}

type _ encoding =
  | Enc : 'a direct_encoding -> 'a encoding
  | Mu : ('a encoding -> 'a encoding) -> 'a encoding

let read s = {Reader.s = s; offset = 0}

let construct enc x = match enc with
  | Enc {ser; _ } -> ser x
  | Mu f ->
    match f enc with
    | Enc {ser; _} -> ser x
    | _ -> Error `recursive_error

let destruct enc x = match enc with
  | Enc {de; _ } -> de x
  | Mu f -> match f enc with
    | Enc {de; _} -> de x
    | _ -> Error `recursive_error

let size enc x = match enc with
  | Enc {size; _} -> size x
  | Mu f -> match f enc with
    | Enc {size; _} -> size x
    | _ -> (fun _ -> -1) x

let eval_size enc x = size enc x

let ok f x = Ok (f x)

let unit = Enc { ser = Writer.unit; de = Reader.unit; size = Size.unit }
let empty = unit
let constant (_s : string) = unit
let int = Enc { ser = Writer.int; de = Reader.int; size = Size.int }
let int32 = Enc { ser = Writer.int32; de = Reader.int32; size = Size.int32 }
let int53 = Enc { ser = Writer.int64; de = Reader.int64; size = Size.int64 }
let bool = Enc { ser = Writer.bool; de = Reader.bool; size = Size.bool }
let string = Enc { ser = Writer.string; de = Reader.string; size = Size.string }
let bytes = Enc { ser = Writer.bytes; de = Reader.bytes; size = Size.bytes }
let hex = Enc { ser = Writer.hex; de = Reader.hex; size = Size.hex }
let option enc = Enc { ser = Writer.option (construct enc); de = Reader.option (destruct enc); size = Size.option (size enc)}
let list enc =
  Enc { ser = Writer.list (construct enc); de = Reader.list (destruct enc); size = Size.list (size enc) }
let array enc = Enc { ser = Writer.array (construct enc); de = Reader.array (destruct enc); size = Size.array (size enc)}
let any len = Enc { ser = Writer.any len; de = Reader.any len; size = Size.any len }

let string_enum l =
  let ser x =
    let rec aux i = function
      | [] -> Error (`no_case_matched None)
      | (_, y) :: _ when y = x -> construct int i
      | _ :: q -> aux (i+1) q in
    aux 0 l in
  let de s =
    let| i, s = destruct int s in
    match List.nth_opt l i with
    | Some (_, x) -> Ok (x, s)
    | _ -> Error (`no_case_matched (Some i)) in
  Enc { ser; de; size = Size.int }

let merge_objs enc1 enc2 =
  let ser (x, y) =
    let b1 = construct enc1 x in
    let b2 = construct enc2 y in
    Writer.concat [b1; b2] in
  let de s =
    let| x, s = destruct enc1 s in
    let| y, s = destruct enc2 s in
    Ok ((x, y), s) in
  Enc { ser; de; size = (fun (x, y) -> (size enc1 x) + (size enc2 y)) }

let merge_tups = merge_objs

let conv of_ to_ enc =
  let ser x = construct enc (of_ x) in
  let de s =
    let| x, s = destruct enc s in
    Ok (to_ x, s) in
  let size x = size enc (of_ x) in
  Enc { ser ; de; size }

let mu _name ?title:_ ?description:_ f =  Mu f

let def _name ?title:_ ?description:_ enc = enc

type 'a field = {enc : 'a encoding; name : string}

let req ?title:_ ?description:_ name enc = {enc; name}
let opt ?title:_ ?description:_ name enc = {enc = option enc; name}
let dft ?title:_ ?description:_ ?construct:_ name enc def =
  let enc_opt = option enc in
  let ser x =
    if def = x then construct enc_opt None else construct enc_opt (Some x) in
  let de s =
    let| x, s = destruct enc_opt s in
    match x with None -> Ok (def, s) | Some x -> Ok (x, s) in
  let size x =
      if def = x then 1
      else 1 + size enc x in
  {enc = Enc { ser; de ; size }; name}

let obj1 f1 = f1.enc
let obj2 f1 f2 = merge_objs (obj1 f1) (obj1 f2)
let obj3 f1 f2 f3 =
  conv (fun (x1, x2, x3) -> x1, (x2, x3)) (fun (x1, (x2, x3)) -> x1, x2, x3)
    (merge_objs (obj1 f1) (obj2 f2 f3))
let obj4 f1 f2 f3 f4 =
  conv (fun (x1, x2, x3, x4) -> x1, (x2, x3, x4))
    (fun (x1, (x2, x3, x4)) -> x1, x2, x3, x4)
    (merge_objs (obj1 f1) (obj3 f2 f3 f4))
let obj5 f1 f2 f3 f4 f5 =
  conv (fun (x1, x2, x3, x4, x5) -> x1, (x2, x3, x4, x5))
    (fun (x1, (x2, x3, x4, x5)) -> x1, x2, x3, x4, x5)
    (merge_objs (obj1 f1) (obj4 f2 f3 f4 f5))
let obj6 f1 f2 f3 f4 f5 f6 =
  conv (fun (x1, x2, x3, x4, x5, x6) -> x1, (x2, x3, x4, x5, x6))
    (fun (x1, (x2, x3, x4, x5, x6)) -> x1, x2, x3, x4, x5, x6)
    (merge_objs (obj1 f1) (obj5 f2 f3 f4 f5 f6))
let obj7 f1 f2 f3 f4 f5 f6 f7 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7) -> x1, (x2, x3, x4, x5, x6, x7))
    (fun (x1, (x2, x3, x4, x5, x6, x7)) -> x1, x2, x3, x4, x5, x6, x7)
    (merge_objs (obj1 f1) (obj6 f2 f3 f4 f5 f6 f7))
let obj8 f1 f2 f3 f4 f5 f6 f7 f8 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7, x8) -> x1, (x2, x3, x4, x5, x6, x7, x8))
    (fun (x1, (x2, x3, x4, x5, x6, x7, x8)) -> x1, x2, x3, x4, x5, x6, x7, x8)
    (merge_objs (obj1 f1) (obj7 f2 f3 f4 f5 f6 f7 f8))
let obj9 f1 f2 f3 f4 f5 f6 f7 f8 f9 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9) -> x1, (x2, x3, x4, x5, x6, x7, x8, x9))
    (fun (x1, (x2, x3, x4, x5, x6, x7, x8, x9)) -> x1, x2, x3, x4, x5, x6, x7, x8, x9)
    (merge_objs (obj1 f1) (obj8 f2 f3 f4 f5 f6 f7 f8 f9))
let obj10 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) -> x1, (x2, x3, x4, x5, x6, x7, x8, x9, x10))
    (fun (x1, (x2, x3, x4, x5, x6, x7, x8, x9, x10)) -> x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
    (merge_objs (obj1 f1) (obj9 f2 f3 f4 f5 f6 f7 f8 f9 f10))

let tup1 enc = enc
let tup2 f1 f2 = merge_tups (tup1 f1) (tup1 f2)
let tup3 f1 f2 f3 =
  conv (fun (x1, x2, x3) -> x1, (x2, x3)) (fun (x1, (x2, x3)) -> x1, x2, x3)
    (merge_tups (tup1 f1) (tup2 f2 f3))
let tup4 f1 f2 f3 f4 =
  conv (fun (x1, x2, x3, x4) -> x1, (x2, x3, x4))
    (fun (x1, (x2, x3, x4)) -> x1, x2, x3, x4)
    (merge_tups (tup1 f1) (tup3 f2 f3 f4))
let tup5 f1 f2 f3 f4 f5 =
  conv (fun (x1, x2, x3, x4, x5) -> x1, (x2, x3, x4, x5))
    (fun (x1, (x2, x3, x4, x5)) -> x1, x2, x3, x4, x5)
    (merge_tups (tup1 f1) (tup4 f2 f3 f4 f5))
let tup6 f1 f2 f3 f4 f5 f6 =
  conv (fun (x1, x2, x3, x4, x5, x6) -> x1, (x2, x3, x4, x5, x6))
    (fun (x1, (x2, x3, x4, x5, x6)) -> x1, x2, x3, x4, x5, x6)
    (merge_tups (tup1 f1) (tup5 f2 f3 f4 f5 f6))
let tup7 f1 f2 f3 f4 f5 f6 f7 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7) -> x1, (x2, x3, x4, x5, x6, x7))
    (fun (x1, (x2, x3, x4, x5, x6, x7)) -> x1, x2, x3, x4, x5, x6, x7)
    (merge_tups (tup1 f1) (tup6 f2 f3 f4 f5 f6 f7))
let tup8 f1 f2 f3 f4 f5 f6 f7 f8 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7, x8) -> x1, (x2, x3, x4, x5, x6, x7, x8))
    (fun (x1, (x2, x3, x4, x5, x6, x7, x8)) -> x1, x2, x3, x4, x5, x6, x7, x8)
    (merge_tups (tup1 f1) (tup7 f2 f3 f4 f5 f6 f7 f8))
let tup9 f1 f2 f3 f4 f5 f6 f7 f8 f9 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9) -> x1, (x2, x3, x4, x5, x6, x7, x8, x9))
    (fun (x1, (x2, x3, x4, x5, x6, x7, x8, x9)) -> x1, x2, x3, x4, x5, x6, x7, x8, x9)
    (merge_tups (tup1 f1) (tup8 f2 f3 f4 f5 f6 f7 f8 f9))
let tup10 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 =
  conv (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) -> x1, (x2, x3, x4, x5, x6, x7, x8, x9, x10))
    (fun (x1, (x2, x3, x4, x5, x6, x7, x8, x9, x10)) -> x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
    (merge_tups (tup1 f1) (tup9 f2 f3 f4 f5 f6 f7 f8 f9 f10))

let assoc enc =
  let ser l =
    Writer.list (fun (label, x) ->
        Writer.concat [construct string label; construct enc x]) l in
  let de s =
    Reader.list (fun s ->
        let| label, s = destruct string s in
        let| x, s = destruct enc s in
        Ok ((label, x), s)) s in
  let size l =
    List.fold_left (fun acc (k, v) ->
        let k = 4 + String.length k in
        k + acc + size enc v) 4 l in
  Enc { ser; de; size }

type _ case = Case : { enc : 'a encoding; proj : 't -> 'a option; inj : 'a -> 't } -> 't case

let case ?title:_ ?description:_ enc proj inj =
  Case { enc; proj; inj }

let union l =
  let ser x =
    let rec aux i = function
      | [] -> Error (`no_case_matched None)
      | Case { enc; proj; _ } :: q ->
        match proj x with
        | None -> aux (i+1) q
        | Some x ->
          Writer.concat [Writer.uint8 i; construct enc x] in
    aux 0 l in
  let de s =
    let| i, s = Reader.uint8 s in
    match List.nth_opt l i with
    | None -> Error (`no_case_matched (Some i))
    | Some (Case {inj; enc; _}) ->
      let| x, s = destruct enc s in
      Ok (inj x, s) in
  let size x =
    let rec aux i = function
      | [] -> -1
      | Case { enc; proj; _ } :: q -> match proj x with
        | None -> aux (i+1) q
        | Some x -> size enc x
    in aux 0 l in
  Enc { ser; de; size }

(* proto *)
let uint64 = Enc {ser=Writer.uint64; de=Reader.uint64; size=Size.uint64}
let uzarith = Enc {ser=Writer.uzarith; de=Reader.uzarith; size=Size.uzarith}
let zarith = Enc {ser=Writer.zarith; de=Reader.zarith; size=Size.zarith}
let pkh = Enc {ser=Writer.pkh; de=Reader.pkh; size=Size.pkh}
let contract = Enc {ser=Writer.contract; de=Reader.contract; size=Size.contract}
let pk = Enc {ser=Writer.pk; de=Reader.pk; size=Size.pk}
let block_hash = Enc {ser=Writer.block_hash; de=Reader.block_hash; size=Size.block_hash}
let signature = Enc {ser=Writer.signature; de=Reader.signature; size=Size.signature}
let script_expr_hash = Enc {ser=Writer.script_expr_hash; de=Reader.script_expr_hash; size=Size.script_expr_hash}
let operations_hash = Enc {ser=Writer.operations_hash; de=Reader.operations_hash; size=Size.operations_hash}
let operation_hash = Enc {ser=Writer.operation_hash; de=Reader.operation_hash; size=Size.operation_hash}
let context_hash = Enc {ser=Writer.context_hash; de=Reader.context_hash; size=Size.context_hash}
let nonce_hash = Enc {ser=Writer.nonce_hash; de=Reader.nonce_hash; size=Size.nonce_hash}
let protocol_hash = Enc {ser=Writer.protocol_hash; de=Reader.protocol_hash; size=Size.protocol_hash}
let chain_id = Enc {ser=Writer.chain_id; de=Reader.chain_id; size=Size.chain_id}
let tx_rollup_hash = Enc {ser=Writer.tx_rollup_hash; de=Reader.tx_rollup_hash; size=Size.tx_rollup_hash}
let tx_inbox_hash = Enc {ser=Writer.tx_inbox_hash; de=Reader.tx_inbox_hash; size=Size.tx_inbox_hash}
let tx_message_result_hash = Enc {ser=Writer.tx_message_result_hash; de=Reader.tx_message_result_hash; size=Size.tx_message_result_hash}
let tx_commitment_hash = Enc {ser=Writer.tx_commitment_hash; de=Reader.tx_commitment_hash; size=Size.tx_commitment_hash}
let sc_rollup_hash = Enc {ser=Writer.sc_rollup_hash; de=Reader.sc_rollup_hash; size=Size.sc_rollup_hash}
let sc_commitment_hash = Enc {ser=Writer.sc_commitment_hash; de=Reader.sc_commitment_hash; size=Size.sc_commitment_hash}
let sc_state_hash = Enc {ser=Writer.sc_state_hash; de=Reader.sc_state_hash; size=Size.sc_state_hash}
let l2_address = Enc {ser=Writer.l2_address; de=Reader.l2_address; size=Size.l2_address}
