(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Raw

(* 32 *)
let block_hash = mk "\001\052" (* B(51) *)
let operation_hash = mk "\005\116" (* o(51) *)
let operation_list_hash = mk "\133\233" (* Lo(52) *)
let operation_list_list_hash = mk "\029\159\109" (* LLo(53) *)
let protocol_hash = mk "\002\170" (* P(51) *)
let context_hash = mk"\079\199" (* Co(52) *)
let script_expr_hash = mk "\013\044\064\027" (* expr(54) *)
let tx_inbox_hash = mk "\079\148\196" (* txi(53) *)
let tx_message_result_hash = mk "\018\007\206\087" (* txmr(54) *)
let tx_commitment_hash = mk "\079\148\017" (* txc(53) *)
let sc_commitment_hash = mk "\017\144\021\100" (* scc1(54) *)
let sc_state_hash = mk "\017\144\122\202" (* scs1(54) *)

(* 20 *)
let ed25519_public_key_hash = mk "\006\161\159" (* tz1(36) *)
let secp256k1_public_key_hash = mk "\006\161\161" (* tz2(36) *)
let p256_public_key_hash = mk "\006\161\164" (* tz3(36) *)
let bls12381_public_key_hash = mk "\006\161\166" (* tz4(36) *)
let contract_public_key_hash = mk "\002\090\121" (* KT1(36) *)
let tx_rollup_hash = mk "\001\128\120\031" (* txr1(37) *)
let sc_rollup_hash = mk "\001\118\132\217" (* scr1(37) *)
let l2_address = mk "\006\161\166" (* tz4(36) *)

let pkh s =
  let s = (s :> string) in
  if String.length s < 3 then None
  else match String.sub s 0 3 with
    | "tz1" -> Some ed25519_public_key_hash
    | "tz2" -> Some secp256k1_public_key_hash
    | "tz3" -> Some p256_public_key_hash
    | "KT1" -> Some contract_public_key_hash
    | _ -> None

(* 16 *)
let cryptobox_public_key_hash = mk "\153\103" (* id(30) *)

(* 32 *)
let ed25519_seed = mk "\013\015\058\007" (* edsk(54) *)
let ed25519_public_key = mk "\013\015\037\217" (* edpk(54) *)
let secp256k1_secret_key = mk "\017\162\224\201" (* spsk(54) *)
let p256_secret_key = mk "\016\081\238\189" (* p2sk(54) *)
let bls12381_secret_key = mk "\003\150\192\040" (* BLsk(54) *)

(* 56 *)
let ed25519_encrypted_seed = mk "\007\090\060\179\041" (* edesk(88) *)
let secp256k1_encrypted_secret_key = mk "\009\237\241\174\150" (* spesk(88) *)
let p256_encrypted_secret_key = mk "\009\048\057\115\171" (* p2esk(88) *)

(* 33 *)
let secp256k1_public_key = mk "\003\254\226\086" (* sppk(55) *)
let p256_public_key = mk "\003\178\139\127" (* p2pk(55) *)
let secp256k1_scalar = mk "\038\248\136" (* SSp(53) *)
let secp256k1_element = mk "\005\092\000" (* GSp(54) *)

let pk s =
  let s = (s :> string) in
  if String.length s < 4 then None
  else match String.sub s 0 4 with
    | "edpk" -> Some ed25519_public_key
    | "sppk" -> Some secp256k1_public_key
    | "p2pk" -> Some p256_public_key
    | _ -> None

(* 64 *)
let ed25519_secret_key = mk "\043\246\078\007" (* edsk(98) *)
let ed25519_signature = mk "\009\245\205\134\018" (* edsig(99) *)
let secp256k1_signature =  mk "\013\115\101\019\063" (* spsig1(99) *)
let p256_signature = mk "\054\240\044\052" (* p2sig(98) *)
let generic_signature = mk "\004\130\043" (* sig(96) *)

let sk ?(only_ed25519=true) s =
  let s = (s :> string) in
  if String.length s < 4 then None
  else match String.sub s 0 4 with
    | "edsk" ->
      if String.length s = 54 then Some (ed25519_seed, true)
      else if String.length s = 98 then Some (ed25519_secret_key, false)
      else None
    | "spsk" when not only_ed25519 -> Some (secp256k1_secret_key, true)
    | "p2sk" when not only_ed25519 -> Some (p256_secret_key, true)
    | _ -> None

let csig s =
  let s = (s :> string) in
  let n = String.length s in
  if n = 96 && String.sub s 0 3 = "sig" then Some generic_signature
  else if n = 98 && String.sub s 0 5 = "p2sig" then Some p256_signature
  else if n = 99 && String.sub s 0 5 = "edsig" then Some ed25519_signature
  else if n = 99 && String.sub s 0 6 = "spsig1" then Some secp256k1_signature
  else None

(* 4 *)
let chain_id = mk "\087\082\000" (* Net(15) *)
(* 32 *)
let nonce_hash = mk "\069\220\169" (* nce(53) *)

(* 48 *)
let bls12381_public_key = mk "\006\149\135\204" (* BLpk(76) *)

(* 96 *)
let bls12381_signature = mk "\040\171\064\207" (* BLsig(142) *)
