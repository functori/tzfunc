let tzfunc_id = Math.floor(Math.random() * 2147483647)
let tzfunc_cbs = {}
const api_src = "api"
const background_src = "background"
const client_src = "client"

window.addEventListener("message", function(ev) {
  if (ev.source == window && ev.data.id && tzfunc_cbs[ev.data.id] && (ev.data.src==background_src || ev.data.src==client_src)) {
    if (ev.data.ok) return tzfunc_cbs[ev.data.id]({ok: ev.data.output});
    else tzfunc_cbs[ev.data.id]({error: ev.data.output})
  }
}, false);

tzfunc = {
  request: function(input, cb) {
    tzfunc_id = (tzfunc_id + 1) % 2147483647
    let request = { id: tzfunc_id, input, src: api_src };
    if (cb != undefined) {
      tzfunc_cbs[tzfunc_id] = cb
      window.postMessage(request, "*")
    } else {
      let p = new Promise(function(resolve, reject) {
        window.addEventListener("message", function(ev) {
          try {
            if (ev.source == window && ev.data.id == request.id && (ev.data.src==background_src || ev.data.src==client_src)) {
              if (ev.data.ok) resolve(ev.data.output);
              else reject(ev.data.output)
            }
          } catch(error) { reject(error) }
        }, false);
        window.postMessage(request, "*")
      });
      return p
    }
  }
}

console.log('tzfunc injected')
