open Ext
open Etypes
open Ezjs_min
open Tzfunc.Rp
include Jext_lwt.Client.Lib(Etypes)

let rrun f =
  EzLwtSys.run @@ fun () ->
  Lwt.map (Result.iter_error print_error) @@
  f ()

let runa0 f app =
  EzLwtSys.run (fun () -> f app)
let runa1 f app x =
  EzLwtSys.run (fun () -> f app x)

let mk_request req =
  {req; account=None; network=None}

let get_sk ~password account =
  let open Crypto in
  let esk = Base58.decode ~prefix:Prefix.ed25519_encrypted_seed account.a_edesk in
  match Box.decrypt ~count:4096 ~password (esk :> string) with
  | None -> Lwt.return_error (`generic ("decrypt_error", "cannot decrypt encrypted secret key with this password"))
  | Some sk -> Lwt.return_ok (Sk.T.mk @@ Raw.mk sk)

let get_sign ?watermark ~password account =
  let>? sk = get_sk ~password account in
  let sign b = match Crypto.Ed25519.sign_bytes ?watermark ~sk b with
    | Error e -> Lwt.return_error e
    | Ok b -> Lwt.return_ok (b :> Crypto.Raw.t) in
  Lwt.return_ok sign

let contract_validity s =
  try String.sub s 0 3 = "KT1" with _ -> false

let account_validity s =
  try String.sub s 0 2 = "tz" with _ -> false

let address_validity ?(contract=false) s =
  let n = String.length s in
  if n = 0 then None
  else if n <> 36 then Some false
  else if contract_validity s then Some true
  else if account_validity s && not contract then Some true
  else Some false

module CopyEmpty = struct
  let%prop value = ""
  and variant = ""
  and placement : (string option [@opt]) = None
  let%data copied = false

  let%meth copy app =
    let tp = Unsafe.global##.bootstrap##._Tooltip##getInstance [%el app] in
    ignore @@ Unsafe.global##.self##.navigator##.clipboard##writeText app##.value;
    match Opt.to_option tp with
    | None -> ()
    | Some tp ->
      app##.copied := _true;
      ignore @@ tp##setContent (Unsafe.obj [| ".tooltip-inner", Unsafe.inject (string "copied!") |]);
      EzLwtSys.run @@ fun () ->
      let|> () = EzLwtSys.sleep 2. in
      ignore @@ tp##setContent (Unsafe.obj [| ".tooltip-inner", Unsafe.inject (string "copy") |]);
      app##.copied := _false

  [%%mounted fun app ->
    let cs : _ constr = Unsafe.global##.bootstrap##._Tooltip in
    match Optdef.to_option [%el app] with
    | None -> ()
    | Some elt ->
      let _ = new%js cs elt in
      ()]

  [%%comp {name="v-copy-empty"; conv}]

  {%%template|
  <button @click="copy()" :class="'btn btn-sm btn-'+variant" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="copy" :data-bs-placement="placement">
    <slot :copied="copied">
    </slot>
  </button>
  |}
end

module CopyButton = struct

  let%prop value = ""
  and variant = ""
  and placement : (string option [@opt]) = None

  [%%comp {name="v-copy-button"; conv}]

  {%%template|
  <v-copy-empty :value="value" :variant="variant" :placement="placement">
    <template #default="{copied}">
      <i v-if="!copied" class="bi bi-clipboard"></i>
      <i v-else class="bi bi-clipboard-check"></i>
    </template>
  </v-copy-empty>
  |}
end

module CopyContainer = struct

  let%prop value = ""
  and variant = ""
  and alignment = "start"
  and placement : (string option [@opt]) = None

  [%%comp {name="v-copy"; conv}]

  {%%template|
  <div style="position:relative">
    <div style="z-index:10;overflow-x:auto" :class="'text-'+alignment">
      <slot></slot>
    </div>
    <v-copy-button :variant="variant" :value="value" :placement="placement" style="position:absolute;top:0;right:0">
    </v-copy-button>
  </div>
  |}

end

module ClearInput = struct

  let%prop [@noconv] value : Unsafe.any = {req}
  and valid : bool option = None
  and type_ = "text"
  and size = "default"
  and autofocus = false
  and offset = 0.
  and opt = false

  let size_class app s =
    match to_string app##.size with "sm" -> s ^ "-sm" | "lg" -> s ^ "-lg" | _ -> ""

  let%comp input_class app : string =
    (size_class app "form-control") ^
    Option.fold ~none:"" ~some:(fun b -> if to_bool b then " is-valid" else " is-invalid") @@
    Optdef.to_option app##.valid
  and btn_class app : string =
    match to_string app##.size with "sm" -> "btn-sm" | "lg" -> "btn-lg" | _ -> ""
  and btn_style app : string =
    let right =
      float_of_number app##.offset +.
      if to_string app##.type_ = "number" || Option.is_some (Optdef.to_option app##.valid) then 2.25 else 1. in
    let top = match to_string app##.size with "sm" -> 0.3 | "lg" -> 0.6 | _ -> 0.45 in
    Format.sprintf "right:%frem;top:%frem;z-index:10; padding:0" right top

  let%meth clear app (_ev: Unsafe.any) =
    let s = if not (to_bool app##.opt) && to_string app##.type_ = "number" then "0" else "" in
    [%emit "update" app (string s)]
  and input app (ev: Unsafe.any) = [%emit "update" app (Unsafe.coerce ev)##.target##.value]
  and enter app (ev: Unsafe.any) = [%emit "enter" app ev]

  [%%comp {name="v-clear"; conv}]

  {%%template|
  <div class="position-relative">
    <slot name="before"></slot>
    <input class="form-control" :value="value" @input="input" @keyup.enter="enter" @keyup.esc="ev => clear(ev)" :class="input_class" :type="type" :autofocus="autofocus">
    <button v-if="!(value=='' || (type=='number' && !opt && value==0))" class="btn btn-light position-absolute" :class="btn_class" @click="ev => clear(ev)" :style="btn_style">
      <i class="bi bi-x"></i>
    </button>
    <slot name="after"></slot>
  </div>
  |}
end
