{%%template|
<v-clear :class="'input-group input-group-'+size" :value="value_aux" @update="change_value" @enter="enter" :size="size" :offset="4" :opt="opt" type="number">
  <template #after>
    <button class="dropdown-toggle btn btn-outline-secondary" data-bs-toggle="dropdown"><span class="px-3">{{ unit }}</span></button>
    <ul class="dropdown-menu dropdown-menu-end">
      <li><a class="dropdown-item" @click="change_unit('ꜩ')" role="button">ꜩ</a></li>
      <li><a class="dropdown-item" @click="change_unit('μꜩ')" role="button">μꜩ</a></li>
    </ul>
  </template>
</v-clear>
|}

open Ezjs_min

let%prop size = ""
and value = ""
and opt = false

let%data [@noconv] value_aux app : Proto_jsoo.A.uint64_jsoo t =
  if to_bool app##.opt then string "" else string "0"
and unit = "\234\156\169"

let%meth [@noconv] enter app (ev: Unsafe.any) = [%emit "enter" app ev]

and [@noconv] change_value app (v: js_string t) =
  app##.value_aux_ := v;
  if to_string (Unsafe.coerce v) = "" then [%emit "update" app undefined]
  else [%emit "update" app (def (object%js val value = v val unit = app##.unit end))]
and [@noconv] change_unit app (unit: js_string t) =
  app##.unit := unit;
  [%emit "update" app (def (object%js val value = app##.value_aux_ val unit = unit end))]

[%%comp {name="v-tz"; conv}]

type t = { value: Proto.A.uint64; unit: string } [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

let abs tz =
  if tz.unit = "\234\156\169" then Int64.mul tz.value 1000000L else tz.value
