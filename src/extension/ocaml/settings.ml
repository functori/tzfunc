{%%template|
<div>
  <div class="accordion" id="accordion-settings">

    <div class="accordion-item">
      <h2 class="accordion-header">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-network">
          Network
        </button>
      </h2>
      <div id="accordion-network" class="accordion-collapse collapse" data-bs-parent="#accordion-settings">
        <div class="accordion-body">
          <div class="input-group">
            <select v-model="network_select" class="form-select form-select-sm">
              <option>mainnet</option>
              <option>ghostnet</option>
              <option>custom</option>
            </select>
            <input v-if="network_select=='custom'" @keyup.enter="set_network(undefined)" class="form-control form-control-sm" placeholder="http://localhost:4242">
          </div>
          <button @click="set_network(undefined)" class="my-2 btn btn-secondary">Set</button>
        </div>
      </div>
    </div>

    <div class="accordion-item">
      <h2 class="accordion-header">
        <button class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-accounts">
          Accounts
        </button>
      </h2>
      <div id="accordion-accounts" class="accordion-collapse collapse" data-bs-parent="#accordion-settings">
        <div class="accordion-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">name</th>
                <th scope="col">tz1</th>
                <th scope="col">edpk</th>
                <th scope="col">actions</th>
              </tr>
            </thead>
            <tbody>
              <template v-for="(a, i) in info.accounts" :key="i">
                <tr>
                  <td>
                    <input v-model="a.name" class="form-control form-control-sm">
                  </td>
                  <td>
                    <v-copy-empty :value="a.tz1">{{ a.tz1.substring(0,10) }}</v-copy-empty>
                  </td>
                  <td>
                    <v-copy-empty :value="a.edpk">{{ a.edpk.substring(0,10) }}</v-copy-empty>
                  </td>
                  <td>
                    <div class="btn-group">
                      <button @click="remove_account(a)" class="btn btn-sm btn-secondary" title="remove">
                        <i class="bi bi-trash-fill"></i>
                      </button>
                      <button @click="update_account(a)" class="btn btn-sm btn-secondary" title="update">
                        <i class="bi bi-arrow-counterclockwise"></i>
                      </button>
                      <button @click="a.showing=!a.showing" class="btn btn-sm btn-secondary" title="show secret key">
                        <i class="bi bi-eye-fill" v-if="!a.showing"></i>
                        <i class="bi bi-eye-slash-fill" v-else></i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr v-if="a.showing">
                  <td colspan="4">
                    <div v-if="a.edsk==undefined" class="d-flex">
                      <div class="input-group input-group-sm d-flex justify-content-center">
                        <div class="input-group-text">Password</div>
                        <input v-model="password" @keyup.enter="show_secret(a)" style="max-width:500px" type="password" autofocus>
                        <button v-if="!processing" @click="show_secret(a)" class="btn btn-secondary">Show</button>
                        <button v-else disabled class="btn btn-secondary">
                          <div class="spinner-border spinner-border-sm"></div>
                        </button>
                      </div>
                    </div>
                    <div v-else>
                      <pre class="d-inline">{{ a.edsk }}</pre>
                      <v-copy-button :value="a.edsk" variant="secondary"></v-copy-button>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="accordion-item">
      <h2 class="accordion-header">
        <button class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-new">
          New Account
        </button>
      </h2>
      <div id="accordion-new" class="accordion-collapse collapse" data-bs-parent="#accordion-settings">
        <div class="accordion-body">
          <v-create @updated="update_info"></v-create>
        </div>
      </div>
    </div>

  </div>
</div>
|}

open Tzfunc
open Ezjs_min
open Rp
open Ext
open Etypes
open State

let%prop info : logged = {req}
and file = ""
and path = ""

let%data network_select = ""
and network_input = ""
and password = ""
and processing = false

let%meth update_info app =
  let account = account_of_jsoo app##.info##.account in
  let network = network_of_jsoo app##.info##.network in
  let|> logged = get_logged ~account ~network false in
  State.emit_logged app (Some logged)

and update_account app (account: account) =
  let> accounts = Storage.update_account account in
  let network = network_of_jsoo app##.info##.network in
  let|> logged = get_logged ~accounts ~account ~network false in
  State.emit_logged app (Some logged)

and remove_account app (account: account) =
  let> accounts = Storage.remove_account account in
  let> r = Storage.get_info accounts in match r with
  | Error _ -> Lwt.return_unit
  | Ok {i_account; i_network} ->
    let|> logged = get_logged ~accounts ~account:i_account ~network:i_network false in
    State.emit_logged app (Some logged)

and set_network app =
  let accounts = to_listf account_of_jsoo app##.info##.accounts in
  let account = account_of_jsoo app##.info##.account in
  set_network ~accounts ~account app

and [@noconv] show_secret app (account: account_jsoo t) =
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let a = account_of_jsoo account in
  let password = to_string app##.password in
  app##.password := string "";
  let> r = Common.get_sk ~password a in
  let () = match r with
    | Error _ -> ()
    | Ok sk -> (Unsafe.coerce account)##.edsk := string (Crypto.Sk.b58enc sk) in
  app##.processing := _false;
  Lwt.return_unit

let%watch path app p _old = match p with
  | "reset" ->
    app##.network_select_ := string "";
    app##.network_input_ := string "";
    app##.password := string "";
    app##.processing := _false
  | _ -> ()

[%%comp {name="v-settings"; conv}]
