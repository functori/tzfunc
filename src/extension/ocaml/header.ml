{%%template|
<nav v-if="file=='index'" class="navbar navbar-expand-sm bg-dark sticky-top" data-bs-theme="dark">
  <a class="navbar-brand px-3" @click="route('reset')">
    <img src="/img/icon.png" alt="Tzfunc" height="40">
  </a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#nav-collapse">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="nav-collapse" class="collapse navbar-collapse">
    <ul class="navbar-nav" v-if="info!=undefined">
      <li class="nav-item">
        <a class="nav-link" @click="route('')" role="button">Transfer</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" @click="route('view')" role="button">View</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" @click="route('delegate')" role="button">Delegate</a>
      </li>
    </ul>
    <ul class="navbar-nav ms-auto" v-if="info!=undefined">
      <li class="nav-item d-flex">
        <v-copy-empty class="ms-2 nav-link" :value="info.account.tz1" variant="link" placement="left">
          <img :src="'https://services.tzkt.io/v1/avatars/'+info.account.tz1" width="28px" class="me-1">
          <span>{{ info.account.name.substring(0, 10) }}</span>
        </v-copy-empty>
        <span v-if="info.node!=undefined" class="navbar-text ms-2">{{ xtz }}</span>
      </li>
      <li class="nav-item dropdown me-2">
        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown"></a>
        <ul class="dropdown-menu dropdown-menu-end">
          <li v-for="a in info.accounts" @click="set_account(a)" :key="a.tz1">
            <a class="dropdown-item">{{ a.name.substring(0, 10) }}</a>
          </li>
        </ul>
      </li>
      <li class="nav-item dropdown mx-2">
        <a class="nav-link dropdown-toggle me-1" data-bs-toggle="dropdown">
          {{ info.network.name || info.network.url }}
        </a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" @click="set_network('mainnet')">mainnet</a></li>
          <li><a class="dropdown-item" @click="set_network('ghostnet')">ghostnet</a></li>
        </ul>
      </li>
    </ul>
    <div v-if="info!=undefined">
      <button class="btn btn-sm btn-dark" @click="route('settings')">
        <i class="bi bi-gear-fill"></i>
      </button>
      <button class="btn btn-sm btn-dark mx-2" @click="route('lock')">
        <i class="bi bi-lock-fill"></i>
      </button>
    </div>
  </div>
</nav>

<div v-else-if="info!==undefined" class="sticky-top">
  <div v-if="info.sign_request===undefined" class="d-flex text-bg-dark" data-bs-theme="dark">
    <span class="p-2">
      <img src="/img/icon.png" alt="home" height="20" @click="route('reset')">
    </span>
    <div class="p-2 flex-grow-1" style="font-size: 0.875rem">{{ info.network.name || info.network.url }}</div>
    <button class="btn btn-dark dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" style="width: 36px"></button>
    <ul class="dropdown-menu">
      <li><a class="dropdown-item" @click="set_network('mainnet')">mainnet</a></li>
      <li><a class="dropdown-item" @click="set_network('ghostnet')">ghostnet</a></li>
    </ul>
  </div>
  <div class="d-flex text-bg-dark" data-bs-theme="dark">
    <span style="width:36px"></span>
    <v-copy-empty class="flex-grow-1" :value="info.account.tz1" variant="dark">
      <img :src="'https://services.tzkt.io/v1/avatars/'+info.account.tz1" width="28px" class="me-1">
      <span class="text-light">{{ info.account.name.substring(0, 10) }}</span>
    </v-copy-empty>
    <button class="btn btn-dark dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" style="width: 36px"></button>
    <ul class="dropdown-menu">
      <li v-for="a in info.accounts" :key="a.tz1">
        <a class="dropdown-item" @click="set_account(a)">
          {{ a.name.substring(0, 10) }}
        </a>
      </li>
    </ul>
  </div>
  <div class="d-flex" v-if="!info.sign_request">
    <button v-if="file=='popup'" class="btn btn-sm btn-dark rounded-0" @click="expand()" title="expand">
      <i class="bi bi-arrows-fullscreen"></i>
    </button>
    <button v-if="file=='popup'" class="btn btn-sm btn-dark rounded-0" @click="route('delegate')" title="delegate">
      <i class="bi bi-caret-up-square-fill"></i>
    </button>
    <div class="flex-fill bg-dark text-light py-1">
      <span v-if="info.node!=undefined">{{ xtz }}</span>
    </div>
    <button v-if="file=='popup'" class="btn btn-sm btn-dark rounded-0" @click="route('settings')" title="settings">
      <i class="bi bi-gear-fill"></i>
    </button>
    <button v-if="file=='popup'" class="btn btn-sm btn-dark rounded-0" @click="route('lock')" title="lock">
      <i class="bi bi-lock-fill"></i>
    </button>
  </div>
</div>
|}

open Tzfunc
open Ezjs_min
open Rp
open Ext
open Etypes
open State

let%prop info : logged option = None
and file = ""

let%data network_select = ""
and network_input = ""

let%meth expand _app =
  let|> _ = Chrome_lwt.Tabs.(create @@ make_create ~url:"index.html" ()) in
  Dom_html.window##close

and set_network app = Settings.set_network (Unsafe.coerce app)

and [@noconv] route app (s: js_string t) =
  [%emit "route" app s]

and set_account app (account: account) =
  match Optdef.to_option app##.info with
  | None -> Lwt.return_unit
  | Some info ->
    let network = network_of_jsoo info##.network in
    let> () = Storage.set_selected account in
    let accounts = to_listf account_of_jsoo info##.accounts in
    let|> logged = get_logged ~accounts ~account ~network true in
    ignore @@ [%emit "logged" app (def @@ logged_to_jsoo logged)]

let%comp xtz app =
  match Optdef.to_option app##.info with
  | None -> undefined
  | Some info ->
    match to_optdef Proto_jsoo.account_of_jsoo info##.node with
    | None -> def @@ string "--"
    | Some account ->
      let cs = Unsafe.global##._Intl##._NumberFormat in
      let intl = new%js cs (string "en-US") in
      let a = (Int64.to_float account.Proto.ac_balance) /. 1000000. in
      def @@ string @@ to_string (intl##format a) ^ " ꜩ"

[%%comp {name="v-header"; conv}]
