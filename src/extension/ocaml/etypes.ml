open Tzfunc
open Ezjs_min_lwt

type raw =
  Tzfunc.Raw.t
  [@class_type js_string]
  [@conv ((fun b -> string (Tzfunc.Crypto.hex_of_raw b :> string)),
          (fun b -> Crypto.hex_to_raw (Tzfunc.H.mk (to_string b))))]
[@@deriving jsoo]

type network_url =
  EzAPI.base_url
  [@class_type js_string]
  [@conv ((fun (EzAPI.BASE s) -> string s),
          (fun s -> EzAPI.BASE (to_string s)))]
[@@deriving jsoo]

type account = {
  a_name: string;
  a_tz1: string;
  a_edpk: string;
  a_edesk: string;
} [@@deriving jsoo]

type network = {
  n_url: network_url;
  n_name: string option;
} [@@deriving jsoo]

type info = {
  i_account: account; [@mutable]
  i_network: network; [@mutable]
} [@@deriving jsoo]

type response_error = Rp.error [@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

type operation_request_kind =
  | Rtransaction of Proto.script_expr Proto.transaction_info
  | Rdelegation of (Proto.A.pkh option [@opt])
  | Rreveal of Proto.A.pk
  | Rorigination of Proto.script_expr Proto.origination_info
  | Rconstant of Proto.script_expr
  | Rdeposits of Proto.A.uint64 option
  | Rtransfer_ticket of Proto.transfer_ticket
[@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type operation_request = {
  source: Proto.A.pkh option;
  kind: operation_request_kind; [@inherit]
  fee: Proto.A.uint64 option;
  gas_limit: Proto.A.zarith option;
  storage_limit: Proto.A.zarith option;
  counter: Proto.A.zarith option;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type response_ok =
  | Rs_rpc of Unsafe.any
  | Rs_sign of string
  | Rs_forge of { branch: string; protocol: string; bytes: raw;
                  operations: operation_request list }
  | Rs_inject of string
  | Rs_send of {
      branch: string; protocol: string; bytes: raw;
      operations : operation_request list; hash: string }
  | Rs_info of { pkh: string; pk: string; network: network }
[@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type sign_request = {
  sir_bytes: raw;
  sir_watermark: raw option;
  sir_type: Proto.micheline option;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type forge_method = [`remote | `local | `both] [@@deriving jsoo {enum}]

type send_request = {
  ser_operations: operation_request list;
  ser_forge_method: forge_method option;
  ser_remove_failed: bool option;
  ser_editable: bool option;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type request_kind =
  | Rq_rpc of string
  | Rq_sign of sign_request
  | Rq_forge of send_request
  | Rq_inject of raw
  | Rq_send of send_request
  | Rq_info
  | Rq_permission of {src: Jext_lwt.Types.request_source; req: request_kind}
  | Rq_callback_ok of response_ok
  | Rq_callback_error of response_error
[@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type request = {
  req: request_kind;
  network: string option;
  account: string option
} [@@deriving jsoo]
