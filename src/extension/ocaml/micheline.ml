{%%template|
<div class="row g-0">
  <div class="col-1 d-grid gap-2" v-if="index!=undefined">
    <button class="btn btn-secondary btn-sm p-1" @click="remove(index)">
      <i class="bi bi-dash-circle-fill"></i>
    </button>
  </div>
  <div v-if="input.typ.bool!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <span v-if="input.name!=undefined" class="input-group-text">
        {{ input.name }}
      </span>
      <select class="form-select" v-model="output.prim">
        <option>True</option>
        <option>False</option>
      </select>
    </div>
  </div>
  <div v-else-if="output.string!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <div v-if="input.name!=undefined" class="input-group-text">{{ input.name }}</div>
      <input v-model="output.string" class="form-control" :placeholder="Object.keys(input.typ)[0]">
    </div>
  </div>
  <div v-else-if="output.bytes!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <div v-if="input.name!=undefined" class="input-group-text">{{ input.name }}</div>
      <input class="form-control" :value="output.bytes" @input="ev=>output.bytes=hex(ev.target.value)" placeholder="bytes">
    </div>
  </div>
  <div v-else-if="output.int!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <div v-if="input.name!=undefined" class="input-group-text">{{ input.name }}</div>
      <input class="form-control" v-model.number="output.int" type="number" :min="(Object.keys(input.typ)[0]!='int') ? 0 : null">
      <div v-if="input.typ.mutez!=undefined" class="input-group-text">μꜩ</div>
    </div>
  </div>
  <div v-else-if="input.typ.tuple!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div v-if="input.name">{{ input.name }}</div>
    <v-micheline v-for="(t, index) in input.typ.tuple" :input="t" :output="output[index]" :key="(input.name || 'tuple')+'-'+index"></v-micheline>
  </div>
  <div v-else-if="input.typ.seq!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div>
      <div class="input-group input-group-sm">
        <div class="input-group-text">{{ input.name || 'seq' }}</div>
        <input disabled>
        <button @click="append(input, output)" class="btn btn-secondary">
          <i class="bi bi-plus-circle-fill"></i>
        </button>
      </div>
      <v-micheline v-for="(v, index) in output" :input="input.typ.seq" :output="v" :key="(input.name || 'seq')+'-'+index" :index="index" :parent="output"></v-micheline>
    </div>
  </div>
  <div v-else-if="input.typ.map!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <div class="input-group-text">{{ input.name || 'map' }}</div>
      <input disabled>
      <button @click="append(input, output)" class="btn btn-secondary">
        <i class="bi bi-plus-circle-fill"></i>
      </button>
    </div>
    <div class="row" v-for="(v, index) in output" :key="(input.name || 'map')+'-'+index">
      <div v-if="index!=undefined" class="col-1 d-grid gap-2">
        <button class="btn btn-secondary btn-sm p-1" @click="remove(output, index)">
          <i class="bi bi-dash-circle-fill"></i>
        </button>
      </div>
      <div :class="'col-'+(index!=undefined ? '11' : '12')">
        <v-micheline :input="input.typ.map[0]" :output="v.args[0]"></v-micheline>
        <v-micheline :input="input.typ.map[1]" :output="v.args[1]"></v-micheline>
      </div>
    </div>
  </div>
  <div v-else-if="input.typ.option!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <div v-if="input.name!=undefined" class="input-group-text">{{ input.name }}</div>
      <select class="form-select" value="none" @change="function(v) { fill(input, output, v) }">
        <option value="none">none</option>
        <option value="some">{{ 'some(' + (input.typ.option.name || Object.keys(input.typ.option.typ)[0]) + ')' }}</option>
      </select>
    </div>
    <v-micheline v-if="output.prim=='Some'" :input="input.typ.option" :output="output.args[0]"></v-micheline>
  </div>
  <div v-else-if="input.typ.or!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')">
    <div class="input-group input-group-sm">
      <div v-if="input.name!=undefined" class="input-group-text">{{ input.name }}</div>
      <select class="form-select" value="none" @change="function(v) { fill(input, output, v) }">
        <option value="left">{{ 'left('+(input.typ.or[0].name || Object.keys(input.typ.or[0].typ)[0])+')' }}</option>
        <option value="right">{{ 'right('+(input.typ.or[1].name || Object.keys(input.typ.or[1].typ)[0])+')' }}</option>
      </select>
    </div>
    <v-micheline :input="(output.prim=='Left') ? input.typ.or[0] : input.typ.or[1]" :output="output.args[0]"></v-micheline>
  </div>
  <div v-else-if="input.typ.unit!=undefined" :class="'col-'+(index!=undefined ? '11' : '12')"></div>
  <div v-else :class="'col-'(index!=undefined ? '11' : '12')">
    <div>Unknown type</div>
    <div>{{ input }}</div>
    <div>{{ output }}</div>
  </div>
</div>
|}

open Ezjs_min
open Tzfunc

let%prop input : Mtyped.ftype = {req}
and output : Proto.micheline = {req}
and index : int option = None
and parent : Proto.micheline list option = None

let rec empty_micheline_value : Mtyped.stype -> Proto.micheline = function
  | `address | `chain_id | `key | `key_hash | `signature | `string | `timestamp
  | `l2_address -> Proto.Mstring ""
  | `bool -> Proto.prim `False
  | `bytes | `chest | `chest_key | `bls12_381_g1 | `bls12_381_g2 -> Proto.Mbytes (H.mk "")
  | `int | `mutez | `nat | `bls12_381_fr -> Proto.Mint Z.zero
  | `map (_k, _v) -> Proto.Mseq []
  | `option _t -> Proto.prim `None
  | `or_ (l, _) -> Proto.prim `Left ~args:[empty_micheline_value l]
  | `seq _t -> Proto.Mseq []
  | `tuple l -> Proto.Mseq (List.map empty_micheline_value l)
  | `unit -> Proto.prim `Unit
  | `big_map _ | `contract _ | `lambda _ | `never | `operation  | `sapling_state _
  | `sapling_transaction _ | `ticket _ ->
    log_str "cannot produce empty michline for this type";
    assert false

let%meth [@noconv] append _app (typ : Mtyped_jsoo.ftype_jsoo t) (a : Proto_jsoo.micheline_jsoo t js_array t) =
  match Mtyped.short @@ Mtyped_jsoo.ftype_of_jsoo typ with
  | `seq t ->
    let v = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value t in
    ignore (a##splice_1 (a##.length) 0 v)
  | `map (k, v) ->
    let k = empty_micheline_value k in
    let v = empty_micheline_value v in
    let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ Proto.Mprim {prim=`Elt; args=[k; v]; annots=[]} in
    ignore (a##splice_1 (a##.length) 0 x)
  | _ -> log_str "cannot append for this input type"

and remove app =
  match Optdef.to_option app##.parent, Optdef.to_option app##.index with
  | Some parent, Some index ->
    let _ = parent##splice index 1 in ()
  | _ -> ()

and [@noconv] fill _app (t : Mtyped_jsoo.ftype_jsoo t) (v : Proto_jsoo.micheline_jsoo t) s =
  let prim = match to_string s, Mtyped_jsoo.ftype_of_jsoo t with
    | "none", {Mtyped.typ=`option _; _} ->
      let _ = (Unsafe.coerce v)##.args##splice 0 1 in
      string "None"
    | "some", {Mtyped.typ=`option t; _} ->
      let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value @@ Mtyped.short t in
      let _ = (Unsafe.coerce v)##.args##splice_1 0 0 x in
      string "Some"
    | "left", {Mtyped.typ=`or_ (l, _); _} ->
      let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value @@ Mtyped.short l in
      let _ = (Unsafe.coerce v)##.args##splice_1 0 1 x in
      string "Left"
    | "right", {Mtyped.typ=`or_ (_, r); _} ->
      let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value @@ Mtyped.short r in
      let _ = (Unsafe.coerce v)##.args##splice_1 0 1 x in
      string "Left"
    | _ -> string "unknown" in
  (Unsafe.coerce v)##.prim := prim

and [@noconv] hex _app (s: js_string t) =
  let s2 = to_string s in
  let t = String.fold_left (fun acc c ->
      acc && (let i = Char.code c in i >= 48 && i <= 57 || i >= 97 && i <= 102)) true s2 in
  if t then s
  else
    let `Hex h = Hex.of_string s2 in string h

[%%comp {name="v-micheline"; conv; modules=[Mtyped, Mtyped_jsoo; Proto, Proto_jsoo]}]
