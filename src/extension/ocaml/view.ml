{%%template|
<div>

  <div class="mb-3">
    <label class="form-label">contract</label>
    <v-clear class="input-group input-group-sm" size="sm" :value="contract" @update="c=>{contract=c; valid.contract=validate(c)}" autofocus :valid="valid.contract" :offset="valid.contract ? 3. : 0.">
      <template #after>
        <button v-if="valid.contract" @click="load_views()" class="btn btn-secondary btn-sm">Load</button>
      </template>
    </v-clear>
  </div>

  <div class="mb-3">
    <label class="form-label">view</label>
    <input v-if="Object.keys(views).length==0" v-model="input" @keyup.enter="view()" class="form-control form-control-sm">
    <select v-else v-model="selected" @keyup.enter="view()" class="form-select form-select-sm">
      <option v-for="k in Object.keys(views)" :key="k">{{ k }}</option>
    </select>
  </div>

  <v-micheline v-if="views[selected]!=undefined" :input="views[selected].input" :output="views[selected].output"></v-micheline>
  <div v-else class="mb-3">
    <label class="form-label">parameters</label>
    <input v-model="param" @keyup.enter="view()" class="form-control form-control-sm">
  </div>

  <div>
    <div class="d-flex align-items-center">
      <div :class="!visible ? 'border flex-grow-1' : 'flex-grow-1'" style="height:0"></div>
      <button class="ml-2 mt-1 btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapse-numbers" @click="visible=!visible">
        <i class="bi bi-caret-down-fill" v-if="!visible" scale="0.6"></i>
        <i class="bi bi-caret-up-fill" v-else scale="0.6"></i>
        Options
      </button>
    </div>
    <div id="collapse-numbers" class="collapse mt-2">
      <div class="mb-3">
        <label class="form-label">amount</label>
        <v-tz size="sm" @update="a=>amount=a" :opt="true"></v-tz>
      </div>
      <div class="mb-3">
        <label class="form-label">balance</label>
        <v-tz size="sm" @update="a=>balance=a" :opt="true"></v-tz>
      </div>
      <div class="mb-3">
        <label class="form-label">source</label>
        <v-clear size="sm" :value="source" @update="c=>{source=c; valid.source=validate(c)}" :valid="valid.source"></v-clear>
      </div>
      <div class="mb-3">
        <label class="form-label">payer</label>
        <v-clear size="sm" :value="payer" @update="c=>{payer=c; valid.payer=validate(c)}" :valid="valid.payer"></v-clear>
      </div>
      <div class="mb-3">
        <label class="form-label">level</label>
        <v-clear size="sm" type="number" :value="level" @update="a=>level=a"></v-clear>
      </div>
      <div class="mb-3">
        <label class="form-label">now</label>
        <v-clear size="sm" :value="now" @update="a=>now=a"></v-clear>
      </div>
    </div>
  </div>

  <button class="btn btn-secondary my-2" :disabled="contract.length!=36 || processing" @click="view()">
    <span v-if="!processing">View</span>
    <div v-else class="spinner-border spinner-border-sm"></div>
  </button>

  <div v-if="result">
    <v-copy :value="JSON.stringify(result)">
      <v-json :data="result"></v-json>
    </v-copy>
  </div>
  <div v-else-if="error">
    <v-copy :value="JSON.stringify(error)">
      <v-json :data="error" :deep="5" deepCollapseChildren></v-json>
    </v-copy>
  </div>

</div>

|}

open Ezjs_min
open Tzfunc
open Rp

type micheline_input = {
  mi_input: Mtyped.ftype;
  mi_output: (Proto.micheline [@conv (Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false, Proto_jsoo.micheline_of_jsoo)]);
  mi_kind: [ `tzip4 | `view of (Proto.micheline * Proto.micheline)];
} [@@deriving jsoo {modules=[Proto, Proto_jsoo; Mtyped, Mtyped_jsoo]}]

type validity = {
  contract: bool option;
  payer: bool option;
  source: bool option;
} [@@deriving jsoo {mut}]

let%data contract = ""
and views : (string * micheline_input) list [@assoc] = []
and input = ""
and selected = ""
and param = ""
and amount = ""
and balance = ""
and source = ""
and payer = ""
and level = ""
and now = ""
and visible = false
and processing = false
and result : Proto.micheline option = None
and error : Rp.error option = None
and valid : validity = {contract=None; payer=None; source=None}

let%meth load_views app =
  let contract = to_string app##.contract in
  let>? l = Tzfunc.Node.get_entrypoints contract in
  let tzip4 = List.filter_map (fun (k, v) ->
      match Mtyped.parse_type v with
      | Error _ -> None
      | Ok mi_input ->
        match mi_input.Mtyped.typ with
        | `tuple [ mi_input; {Mtyped.typ=`contract _; _} ] ->
          let s = Mtyped.short mi_input in
          begin
            try Some (k, {mi_input; mi_output = Micheline.empty_micheline_value s; mi_kind=`tzip4})
            with _ -> None
          end
        | _ -> None) l in
  let|>? a = Tzfunc.Node.get_account_info contract in
  let views = match a.Proto.ac_script with
    | Some {Proto.code=Proto.Mseq l; _} ->
      List.filter_map (function
          | Proto.Mprim {prim=`view; args=[ Proto.Mstring k; input_type; output_type; _]; _} ->
            begin match Mtyped.parse_type input_type with
              | Error _ -> None
              | Ok mi_input ->
                let s = Mtyped.short mi_input in
                try Some (k, {mi_input; mi_output = Micheline.empty_micheline_value s; mi_kind=`view (input_type, output_type)})
                with _ -> None
            end
          | _ -> None) l
    | _ -> [] in
  app##.views := Table.makef micheline_input_to_jsoo (tzip4 @ views)

and view app =
  app##.processing := _true;
  app##.error := undefined;
  app##.result := undefined;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let err e =
    app##.error := def (Rp_jsoo.error_to_jsoo e);
    app##.processing := _false;
    Lwt.return_unit in
  let contract = to_string app##.contract in
  if contract = "" then err @@ `generic ("empty contract", "")
  else
    let re = function
      | Error e ->
        err e
      | Ok m ->
        app##.result := def (Proto_jsoo.micheline_to_jsoo m);
        app##.processing := _false;
        Lwt.return_unit in
    let source = match to_string app##.source with "" -> None | s -> Some s in
    let payer = match to_string app##.payer with "" -> None | s -> Some s in
    let level = Int32.of_string_opt @@ to_string app##.level in
    let now = try Some (Proto.A.cal_of_str @@ to_string app##.now) with _ -> None in
    let selected = to_string app##.selected in
    match to_string app##.input, Table.find app##.views selected with
    | "", None ->
      app##.error := def (Rp_jsoo.error_to_jsoo (`generic ("no entrypoint given", "")));
      app##.processing := _false;
      Lwt.return_unit
    | e, None ->
      begin try
          let input = EzEncoding.destruct Proto.micheline_enc.Proto.json @@ to_string app##.param in
          let> r = Utils.tzip4_view ~contract ~input ?source ?payer ?level ?now e in
          re r
        with exn -> err @@ `generic ("unvalid parameter", Printexc.to_string exn)
      end
    | _, Some mi ->
      let mi = micheline_input_of_jsoo mi in
      match mi.mi_kind with
      | `tzip4 ->
        let> r = Utils.tzip4_view ?source ?payer ?level ?now
            ~contract ~input:mi.mi_output selected in
        re r
      | `view (input_type, output_type) ->
        let amount = Int64.of_string_opt @@ to_string app##.amount in
        let balance = Int64.of_string_opt @@ to_string app##.balance in
        let> r = Utils.view ?balance ?amount ?source ?payer ?level ?now
            ~contract ~input_type ~output_type ~input:mi.mi_output selected in
        re r

and validate _app (s: string) : bool option =
  Common.address_validity ~contract:true s

and [@noconv] update_contract app (s: js_string t) =
  app##.contract := s;
  app##.valid##.contract := optdef bool @@ Common.address_validity ~contract:true @@ to_string s;


[%%comp {name="v-view"; conv; modules = [Proto, Proto_jsoo; Mtyped, Mtyped_jsoo; Rp, Rp_jsoo]}]
