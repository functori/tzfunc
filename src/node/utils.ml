open Proto
open Rp

let transaction ?(fee= -1L) ?(counter=Z.zero) ?(gas_limit=Z.minus_one) ?(storage_limit=Z.minus_one)
    ?(amount=0L) ?metadata ?entrypoint ?param ~source destination =
  let parameters = match entrypoint, param with
    | None, None -> None
    | Some s, None -> Some { entrypoint = EPnamed s; value = Micheline (prim `Unit) }
    | None, Some value -> Some { entrypoint = EPdefault; value }
    | Some s, Some value -> Some { entrypoint = EPnamed s; value } in
  {
    man_info = { source; kind = Transaction { amount; destination; parameters } };
    man_numbers = { fee; counter; gas_limit; storage_limit };
    man_metadata = metadata
  }

let origination ?(fee= -1L) ?(counter=Z.zero) ?(gas_limit=Z.minus_one) ?(storage_limit=Z.minus_one)
    ?(balance=0L) ?metadata ~code ~source storage =
  {
    man_info = { source; kind = Origination { balance; script = { code; storage } } };
    man_numbers = { fee; counter; gas_limit; storage_limit };
    man_metadata = metadata
  }

let delegation ?(fee= -1L) ?(counter=Z.zero) ?(gas_limit=Z.minus_one) ?(storage_limit=Z.minus_one)
    ?metadata ~source delegate =
  {
    man_info = { source; kind = Delegation delegate };
    man_numbers = { fee; counter; gas_limit; storage_limit };
    man_metadata = metadata
  }

let register_constant ?(fee= -1L) ?(counter=Z.zero) ?(gas_limit=Z.minus_one)
    ?(storage_limit=Z.minus_one) ~source code =
  let op = {
    man_info = { source; kind = Constant code };
    man_numbers = { fee; counter; gas_limit; storage_limit };
    man_metadata = None
  } in
  let$ b = match code with
    | Micheline m -> Forge.forge_micheline m
    | Bytes h -> Ok (Crypto.hex_to_raw h) in
  let h = Crypto.(Base58.encode ~prefix:Prefix.script_expr_hash @@ blake2b_32 [ b ]) in
  Ok (op, h)

let send ?base ?get_pk ?remove_failed ?forge_method ?block ?counter_block ~sign ops =
  let>? bytes, protocol, branch, ops =
    Node.forge_manager_operations ?base ?get_pk ?remove_failed ?forge_method
      ?block ?counter_block ops in
  Node.inject ?base ~sign ~bytes ~branch ~protocol ops

let send_select ?base ?get_pk ?remove_failed ?forge_method ?block ?counter_block ~sign ops =
  let>? bytes, protocol, branch, ops, errored =
    Node.forge_manager_operations_select ?base ?get_pk ?remove_failed ?forge_method
      ?block ?counter_block ops in
  let|>? hash = Node.inject ?base ~sign ~bytes ~branch ~protocol ops in
  hash, errored

let wait_next_block ?base ?(offset=1.) () =
  let open CalendarLib.Calendar in
  let>? constants = match Node.config.Node.constants with
    | Some c -> Lwt.return_ok c
    | None -> Node.get_constants ?base () in
  let get_shell () = Node.get_enc_rpc ?base shell_enc.json "/chains/main/blocks/head/header/shell" in
  let>? shell = get_shell () in
  let rec aux () =
    let t = to_unixfloat @@ now () in
    let diff = Float.rem (t -. to_unixfloat shell.timestamp) 120. in
    let> () = EzLwtSys.sleep (constants.time_between_blocks -. diff +. offset) in
    let>? s = get_shell () in
    if s.level <= shell.level then aux ()
    else Lwt.return_ok () in
  aux ()

let rec wait_n_blocks ?base ?offset n =
  if n <= 0 then Lwt.return_ok ()
  else
    let>? () = wait_next_block ?base ?offset () in
    wait_n_blocks ?base ?offset (n-1)

let wait_operation ?base ?(limit=5) ?offset hash =
  let enc =
  Json_encoding.(
    conv (fun ops -> (), (), (), ops) (fun (_, _, _, ops) -> ops) @@
    tup4 unit unit unit
      (list (EzEncoding.ignore_enc (obj1 (req "hash" string))))) in
  let rec aux i =
    let>? operations = Node.get_enc_rpc ?base enc "/chains/main/blocks/head/operations" in
    if List.mem hash operations then Lwt.return_ok ()
    else if i <= 1 then
      Lwt.return_error @@
      `generic ("too_many_iterations",
                Format.sprintf "operation %s not found after %d blocks" hash limit)
    else
      let>? () = wait_next_block ?base ?offset () in
      aux (i-1) in
  aux limit

let get_chain_id ?base ci = match ci, Node.config.Node.chain_id with
  | Some ci, _ | _, Some ci -> Lwt.return_ok ci
  | _ ->
    let|>? h = Node.get_header ?base () in
    h#chain_id

let tzip4_view ?base ?(mode=`Readable) ?chain_id ?source ?payer ?now ?level
    ?(input=prim `Unit) ~contract e =
  let>? chain_id = get_chain_id ?base chain_id in
  let input = object
    method contract = contract method entrypoint = EPnamed e
    method input = input method chain_id = chain_id method unparsing_mode = mode
    method source = source method payer = payer method now = now method level = level
  end in
  Node.post0 ~msg:"run-view" ~input ?base Node.Services.run_view

let view_script ~input_type ~output_type ~contract view =
  let open Node in
  Mseq [
    prim `parameter ~args:[input_type];
    prim `storage ~args:[prim `option ~args:[output_type]];
    prim `code ~args:[ Mseq [
        prim `CAR;
        prim `PUSH ~args:[prim `address; Mstring contract];
        prim `SWAP;
        prim `VIEW ~args:[ Mstring view; output_type ];
        prim `NIL ~args:[ prim `operation ];
        prim `PAIR
      ] ]
  ]

let view ?chain_id ?base ?(amount=0L) ?(balance=0L) ?source ?payer ?now ?level
    ?input_type ?output_type ?(input=prim `Unit) ~contract e =
  let>? chain_id = get_chain_id ?base chain_id in
  let>? input_type, output_type = match input_type, output_type with
    | Some i, Some o -> Lwt.return_ok (i, o)
    | _ ->
      let>? ac = Node.get_account_info ?base contract in
      match ac.ac_script with
      | Some {code=Mseq l; _} ->
        begin match List.find_map (function
            | Proto.Mprim {prim=`view; args=[ Proto.Mstring k; input_type; output_type; _]; _} when k = e ->
              Some (input_type, output_type)
            | _ -> None) l with
        | Some x -> Lwt.return_ok x
        | None -> Lwt.return_error (`generic ("unknown_view", e))
        end
      | _ -> Lwt.return_error (`generic ("contract without code", contract)) in
  let input = object
    method script = view_script ~input_type ~output_type ~contract e
    method storage = prim `None method input = input method amount = amount
    method chain_id = chain_id method balance = balance method entrypoint = None
    method source = source method payer = payer method now = now method level = level
  end in
  let>? r = Node.post0 ~msg:"run-code" ~input ?base Node.Services.run_code in
  match r#storage with
  | Mprim { prim=`Some; args=[ a ]; _} -> Lwt.return_ok a
  | _ -> Lwt.return_error (`unexpected_michelson "not a some in for a view")
