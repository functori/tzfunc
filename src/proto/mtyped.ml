open Proto

let (let$) = Result.bind

let rmap f l =
  let rec aux acc = function
    | [] -> Ok (List.rev acc)
    | t :: q -> match f t with
      | Ok x -> aux (x :: acc) q
      | Error e -> Error e in
  aux [] l

type value = [
  | `address of (string [@wrap "address"])
  | `assoc of ((value [@key "key"]) * (value [@key "value"]) [@object]) list
  | `bls12_381_fr of (Proto.A.zarith [@wrap "bls_fr"])
  | `bls12_381_g1 of (Proto.hex [@wrap "bls_g1"])
  | `bls12_381_g2 of (Proto.hex [@wrap "bls_g2"])
  | `bool of (bool [@wrap "bool"])
  | `bytes of (Proto.hex [@wrap "bytes"])
  | `chain_id of (string [@wrap "chain_id"])
  | `chest of (Proto.hex [@wrap "chest"])
  | `chest_key of (Proto.hex [@wrap "chest_key"])
  | `contract
  | `int of (Proto.A.zarith [@wrap "int"])
  | `key of (string [@wrap "key"])
  | `key_hash of (string [@wrap "key_hash"])
  | `l2_address of (string [@wrap "l2_address"])
  | `lambda of (Proto.micheline [@wrap "lambda"])
  | `left of (value [@wrap "left"])
  | `mutez of (Proto.A.zarith [@wrap "mutez"])
  | `nat of (Proto.A.zarith [@wrap "nat"])
  | `never
  | `none
  | `operation
  | `right of (value [@wrap "right"])
  | `seq of (value list [@wrap "seq"])
  | `sapling_state
  | `sapling_transaction
  | `signature of (string [@wrap "signature"])
  | `some of (value [@wrap "some"])
  | `string of (string [@wrap "string"])
  | `ticket of (Proto.micheline [@wrap "ticket"])
  | `timestamp of (Proto.A.timestamp [@wrap "timestamp"])
  | `tuple of (value list [@wrap "tuple"])
  | `unit
] [@@deriving encoding {recursive}, jsoo {modules=[Proto, Proto_jsoo]}]

type 'a type_aux = [
  | `address
  | `big_map of (('a [@key "bkey"]) * ('a [@key "bvalue"]) [@object])
  | `bls12_381_fr
  | `bls12_381_g1
  | `bls12_381_g2
  | `bool
  | `bytes
  | `chain_id
  | `chest
  | `chest_key
  | `contract of ('a [@wrap "contract"])
  | `int
  | `key
  | `key_hash
  | `l2_address
  | `lambda of (('a [@key "arg"]) * ('a [@key "result"]) [@object])
  | `map of (('a [@key "key"]) * ('a [@key "value"]) [@object])
  | `mutez
  | `nat
  | `never
  | `operation
  | `option of ('a [@wrap "option"])
  | `or_ of (('a [@key "left"]) * ('a [@key "right"]) [@object])
  | `sapling_state of (Proto.A.zarith [@wrap "sapling_state"])
  | `sapling_transaction of (Proto.A.zarith [@wrap "sapling_transaction"])
  | `seq of ('a [@wrap "seq"])
  | `signature
  | `string
  | `timestamp
  | `ticket of ('a [@wrap "ticket"])
  | `tuple of 'a list
  | `unit
] [@@deriving encoding, jsoo {modules=[Proto, Proto_jsoo]}]

type ftype = {
  name: string option;
  typ: ftype type_aux [@json.key "type"]
} [@@deriving encoding {recursive}, jsoo {recursive=true}]

type stype = stype type_aux
[@@deriving encoding {recursive}]

let get_name_annots = function
  | [ s ] when String.length s > 0 && String.get s 0 = '%' ->
    Some (String.sub s 1 (String.length s - 1))
  | _ -> None

let rec parse_type : (micheline -> (ftype, [> `unexpected_michelson of string ]) result) = function
  | Mseq _ | Mint _ | Mstring _ | Mbytes _ -> Error (`unexpected_michelson "type should be a primitive")
  | Mprim { prim; args; annots } ->
    let name = get_name_annots annots in
    match prim, args with
    | `address, _ -> Ok { name; typ=`address }
    | `bool, _ -> Ok { name; typ=`bool }
    | `bytes, _ -> Ok { name; typ=`bytes }
    | `chain_id, _ -> Ok { name; typ=`chain_id }
    | `int, _ -> Ok { name; typ=`int }
    | `key, _ -> Ok { name; typ=`key }
    | `key_hash, _ -> Ok { name; typ=`key_hash }
    | `tx_rollup_l2_address, _ -> Ok { name; typ=`l2_address }
    | `mutez, _ -> Ok { name; typ=`mutez }
    | `nat, _ -> Ok { name; typ=`nat }
    | `operation, _ -> Ok { name; typ=`operation }
    | `signature, _ -> Ok { name; typ=`signature }
    | `string, _ -> Ok { name; typ=`string }
    | `timestamp, _ -> Ok { name; typ=`timestamp }
    | `unit, _ -> Ok { name; typ=`unit }
    | `never, _ -> Ok { name; typ=`never }
    | `bls12_381_fr, _ -> Ok { name; typ=`bls12_381_fr }
    | `bls12_381_g1, _ -> Ok { name; typ=`bls12_381_g1 }
    | `bls12_381_g2, _ -> Ok { name; typ=`bls12_381_g2 }
    | `chest, _ -> Ok { name; typ=`chest }
    | `chest_key, _ -> Ok { name; typ=`chest_key }
    | `sapling_state, [ Mint i ] -> Ok { name; typ=`sapling_state i }
    | `sapling_transaction, [ Mint i ]
    | `sapling_transaction_deprecated, [ Mint i ] -> Ok { name; typ=`sapling_transaction i}
    | `ticket, [ arg ] ->
      let$ t = parse_type arg in
      Ok { name; typ=`ticket t }
    | `option, [ arg ] ->
      let$ t = parse_type arg in
      Ok { name; typ=`option t }
    | `contract, [ arg ] ->
      let$ t = parse_type arg in
      Ok { name; typ=`contract t }
    | `or_, [ l; r ] ->
      let$ l = parse_type l in
      let$ r = parse_type r in
      Ok { name; typ=`or_ (l, r) }
    | `list, [ arg ] | `set, [ arg ] ->
      let$ t = parse_type arg in
      Ok { name; typ=`seq t }
    | `lambda, [ arg; res ] ->
      let$ arg = parse_type arg in
      let$ res = parse_type res in
      Ok { name; typ=`lambda (arg, res) }
    | `map, [ k; v ] ->
      let$ k = parse_type k in
      let$ v = parse_type v in
      Ok { name; typ=`map (k, v) }
    | `big_map, [ k; v ] ->
      let$ k = parse_type k in
      let$ v = parse_type v in
      Ok { name; typ=`big_map (k, v) }
    | `pair, [ arg1; arg2 ] ->
      begin match parse_type arg1, parse_type arg2 with
        | Ok t1, Ok {typ=`tuple l; _} -> Ok { name; typ=`tuple (t1 :: l) }
        | Ok t1, Ok t2 -> Ok { name; typ=`tuple [t1; t2] }
        | Error e, _ | _, Error e -> Error e
      end
    | `pair, (arg1 :: args) ->
      begin match parse_type arg1, parse_type (Mprim {prim=`pair; args; annots=[]}) with
        | Ok t1, Ok {typ=`tuple l; _} -> Ok { name; typ=`tuple (t1 :: l) }
        | Ok t1, Ok t2 -> Ok { name; typ=`tuple [t1; t2] }
        | Error e, _ | _, Error e -> Error e
      end
    | _ -> Error (`unexpected_michelson "primitive type arguments not matching")

let parse_value t m : (value, _) result =
  let rec aux (t : stype) (m : micheline) : (value, _) result = match t, m with
    | `seq t, Mseq l ->
      Result.map (fun l -> `seq l ) @@ rmap (aux t) l
    | `map (k, v), Mseq l | `big_map (k, v), Mseq l ->
      Result.map (fun l -> `assoc l) @@
      rmap (function
          | Mprim { prim = `Elt; args= [m1; m2]; _ } ->
            begin match aux k m1, aux v m2 with
              | Ok x1, Ok x2 -> Ok (x1, x2)
              | Error e, _ | _, Error e -> Error e
            end
          | _ -> Error (`unexpected_michelson "map element is not an elt")) l
    | `big_map (_, _), Mint id -> Ok (`nat id)
    | `string, Mstring s -> Ok (`string s)
    | `key, Mstring s -> Ok (`key s)
    | `key_hash, Mstring s -> Ok (`key_hash s)
    | `address, Mstring s -> Ok (`address s)
    | `l2_address, Mstring s -> Ok (`l2_address s)
    | `signature, Mstring s -> Ok (`signature s)
    | `chain_id, Mstring s -> Ok (`chain_id s)
    | `timestamp, Mstring s -> Ok (`timestamp (A.cal_of_str s))
    | `nat, Mint i -> Ok (`nat i)
    | `int, Mint i -> Ok (`int i)
    | `mutez, Mint i -> Ok (`mutez i)
    | `timestamp, Mint i ->
      Ok (`timestamp (CalendarLib.Calendar.from_unixfloat (Z.to_float i)))
    | `bytes, Mbytes b -> Ok (`bytes b)
    | `key, Mbytes b ->
      Result.map (fun (s, _) -> `key s) @@
      Binary.Reader.(pk {s=Crypto.hex_to_raw b; offset=0})
    | `key_hash, Mbytes b ->
      Result.map (fun (s, _) -> `key_hash s) @@
      Binary.Reader.(pkh {s=Crypto.hex_to_raw b; offset=0})
    | `address, Mbytes b ->
      Result.map (fun (s, _) -> `address s) @@
      Binary.Reader.(contract {s=Crypto.hex_to_raw b; offset=0})
    | `l2_address, Mbytes b ->
      Result.map (fun (s, _) -> `l2_address s) @@
      Binary.Reader.(l2_address {s=Crypto.hex_to_raw b; offset=0})
    | `signature, Mbytes b ->
      Result.map (fun (s, _) -> `signature s) @@
      Binary.Reader.(signature {s=Crypto.hex_to_raw b; offset=0})
    | `chain_id, Mbytes b ->
      Result.map (fun (s, _) -> `chain_id s) @@
      Binary.Reader.(chain_id {s=Crypto.hex_to_raw b; offset=0})
    | `option _, Mprim { prim=`None; args=[]; _} -> Ok `none
    | `unit, Mprim { prim=`Unit; args=[]; _} -> Ok `unit
    | `bool, Mprim { prim=`True; args=[]; _} -> Ok (`bool true)
    | `bool, Mprim { prim=`False; args=[]; _} -> Ok (`bool false)
    | `option t, Mprim { prim = `Some; args=[m]; _} ->
      Result.map (fun x -> `some x) @@ aux t m
    | `or_ (l, _), Mprim { prim = `Left; args=[m]; _} ->
      Result.map (fun x -> `left x) @@ aux l m
    | `or_ (_, r), Mprim { prim = `Right; args=[m]; _} ->
      Result.map (fun x -> `right x) @@ aux r m
    | `tuple [ t1; t2 ], Mseq [m1; m2]
    | `tuple [ t1; t2 ], Mprim {prim=`Pair; args=[m1; m2]; _} ->
      begin match aux t1 m1, aux t2 m2 with
        | Ok v1, Ok v2 -> Ok (`tuple [ v1; v2 ])
        | Error e, _ | _, Error e -> Error e
      end
    | `tuple (t1 :: t), Mprim {prim=`Pair; args=[m1; m2]; _} ->
      begin match aux t1 m1, aux (`tuple t) m2 with
        | Ok v1, Ok (`tuple v) -> Ok (`tuple (v1 :: v))
        | Error e, _ | _, Error e -> Error e
        | _ -> Error (`unexpected_michelson "tail of tuple is not a tuple")
      end
    | `tuple [t1; t2], Mseq (m1 :: m)
    | `tuple [t1; t2], Mprim {prim=`Pair; args=(m1 :: m); _} ->
      begin match aux t1 m1, aux t2 (Mseq m) with
        | Ok v1, Ok (`tuple v) -> Ok (`tuple (v1 :: v))
        | Error e, _ | _, Error e -> Error e
        | _ -> Error (`unexpected_michelson "tail of tuple is not a tuple")
      end
    | `tuple (t1 :: t), Mseq (m1 :: m)
    | `tuple (t1 :: t), Mprim {prim=`Pair; args=(m1 :: m); _} ->
      begin match aux t1 m1, aux (`tuple t) (Mseq m) with
        | Ok v1, Ok (`tuple v) -> Ok (`tuple (v1 :: v))
        | Error e, _ | _, Error e -> Error e
        | _ -> Error (`unexpected_michelson "tail of tuple is not a tuple")
      end
    | `operation, _ -> Ok `operation
    | `contract _, _ -> Ok `contract
    | `lambda _, m -> Ok (`lambda m)
    | `chest, Mbytes h -> Ok (`chest h)
    | `chest_key, Mbytes h -> Ok (`chest_key h)
    | `bls12_381_fr, Mint i -> Ok (`bls12_381_fr i)
    | `bls12_381_fr, Mbytes h ->
      let i = Z.of_string ("0x" ^ (h :> string)) in
      Ok (`bls12_381_fr i)
    | `bls12_381_g1, Mbytes h -> Ok (`bls12_381_g1 h)
    | `bls12_381_g2, Mbytes h -> Ok (`bls12_381_g2 h)
    | `never, Mprim {prim=`NEVER; _} -> Ok `never
    | `ticket _, m -> Ok (`ticket m)
    | _ -> Error (`unexpected_michelson "type and value not matching") in
  aux t m

let rec search_value ~name (t : ftype) (m : value) : value option =
  if t.name = Some name then Some m
  else match t.typ, m with
    | `option t, `some m
    | `or_ (t, _), `left m
    | `or_ (_, t), `right m -> search_value ~name t m
    | `tuple [], `tuple _ | `tuple _, `tuple [] -> None
    | `tuple (ht :: t), `tuple (hm :: m) ->
      begin match search_value ~name ht hm with
        | None -> search_value ~name {name=None;typ=`tuple t} (`tuple m)
        | Some m -> Some m
      end
    | _ -> None

let rec search_values ~names (t : ftype) (m : value) : (string * value) option =
  match List.find_opt (fun n -> t.name = Some n) names with
  | Some n -> Some (n, m)
  | None ->
    match t.typ, m with
    | `option t, `some m
    | `or_ (t, _), `left m
    | `or_ (_, t), `right m -> search_values ~names t m
    | `tuple [], `tuple _ | `tuple _, `tuple [] -> None
    | `tuple (ht :: t), `tuple (hm :: m) ->
      begin match search_values ~names ht hm with
        | None -> search_values ~names {name=None;typ=`tuple t} (`tuple m)
        | Some m -> Some m
      end
    | _ -> None

let rec short (t : ftype) : stype = match t.typ with
  | `address -> `address
  | `big_map (k, v) -> `big_map (short k, short v)
  | `bls12_381_fr -> `bls12_381_fr
  | `bls12_381_g1 -> `bls12_381_g1
  | `bls12_381_g2 -> `bls12_381_g2
  | `bool -> `bool
  | `bytes -> `bytes
  | `chain_id -> `chain_id
  | `chest -> `chest
  | `chest_key -> `chest_key
  | `contract t -> `contract (short t)
  | `int -> `int
  | `key -> `key
  | `key_hash -> `key_hash
  | `l2_address -> `l2_address
  | `lambda (arg, res) -> `lambda (short arg, short res)
  | `mutez -> `mutez
  | `nat -> `nat
  | `never -> `never
  | `operation -> `operation
  | `signature -> `signature
  | `string -> `string
  | `timestamp -> `timestamp
  | `unit -> `unit
  | `map (k, v) -> `map (short k, short v)
  | `option t -> `option (short t)
  | `or_ (l, r) -> `or_ (short l, short r)
  | `sapling_state i -> `sapling_state i
  | `sapling_transaction i -> `sapling_transaction i
  | `ticket t -> `ticket (short t)
  | `seq t -> `seq (short t)
  | `tuple l -> `tuple (List.map short l)

let rec named_micheline_type ?name (t : stype) : ftype = match t with
  | `address -> {typ=`address; name}
  | `bls12_381_fr -> {typ=`bls12_381_fr; name}
  | `bls12_381_g1 -> {typ=`bls12_381_g1; name}
  | `bls12_381_g2 -> {typ=`bls12_381_g2; name}
  | `bool -> {typ=`bool; name}
  | `bytes -> {typ=`bytes; name}
  | `chain_id -> {typ=`chain_id; name}
  | `chest -> {typ=`chest; name}
  | `chest_key -> {typ=`chest_key; name}
  | `contract t -> {typ=`contract (named_micheline_type t); name}
  | `int -> {typ=`int; name}
  | `key -> {typ=`key; name}
  | `key_hash -> {typ=`key_hash; name}
  | `l2_address -> {typ=`l2_address; name}
  | `lambda (arg, res)-> {typ=`lambda (named_micheline_type arg, named_micheline_type res); name}
  | `mutez -> {typ=`mutez; name}
  | `nat -> {typ=`nat; name}
  | `never -> {typ=`never; name}
  | `operation -> {typ=`operation; name}
  | `signature -> {typ=`signature; name}
  | `string -> {typ=`string; name}
  | `timestamp -> {typ=`timestamp; name}
  | `unit -> {typ=`unit; name}
  | `map (k, v) -> {typ=`map (named_micheline_type k, named_micheline_type v); name}
  | `big_map (k, v) -> {typ=`big_map (named_micheline_type k, named_micheline_type v); name}
  | `option t -> {typ=`option (named_micheline_type t); name}
  | `or_ (l, r) -> {typ=`or_ (named_micheline_type l, named_micheline_type r); name}
  | `sapling_state i -> {typ=`sapling_state i; name}
  | `sapling_transaction i -> {typ=`sapling_transaction i; name}
  | `seq t -> {typ=`seq (named_micheline_type t); name}
  | `ticket t -> {typ=`ticket (named_micheline_type t); name}
  | `tuple l -> {typ=`tuple (List.map named_micheline_type l); name}

let rec to_michelson : value -> micheline = function
  | `address a -> Mstring a
  | `assoc l -> Mseq (List.map (fun (k, v) ->
      prim `Elt ~args:[ to_michelson k; to_michelson v ]) l)
  | `bls12_381_fr i -> Mint i
  | `bls12_381_g1 h -> Mbytes h
  | `bls12_381_g2 h -> Mbytes h
  | `bool b -> Mstring (String.capitalize_ascii @@ string_of_bool b)
  | `bytes h -> Mbytes h
  | `chain_id s -> Mstring s
  | `chest h -> Mbytes h
  | `chest_key h -> Mbytes h
  | `contract -> failwith "contract not allowed in parameters"
  | `int z -> Mint z
  | `key s -> Mstring s
  | `key_hash s -> Mstring s
  | `l2_address s -> Mstring s
  | `lambda m -> m
  | `left v -> prim `Left ~args:[to_michelson v]
  | `mutez z -> Mint z
  | `nat z -> Mint z
  | `never -> prim `never
  | `none -> prim `None
  | `operation -> failwith "operation not allowed in parameters"
  | `right v -> prim `Right ~args:[to_michelson v]
  | `sapling_state -> Mseq []
  | `sapling_transaction -> failwith "sapling_transaction not allowed in parameters"
  | `seq l | `tuple l -> Mseq (List.map to_michelson l)
  | `signature s -> Mstring s
  | `some v -> prim `Some ~args:[to_michelson v]
  | `string s -> Mstring s
  | `ticket m -> m
  | `timestamp t -> Mint (Z.of_float @@ CalendarLib.Calendar.to_unixfloat t)
  | `unit -> prim `Unit
