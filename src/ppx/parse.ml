open Micheline_parser

let rec json = function
  | `String s -> Error (Format.sprintf "cannot extract type from %S" s)
  | `Float f -> Error (Format.sprintf "cannot extract type from %f" f)
  | `Bool b -> Error (Format.sprintf "cannot extract type from %B" b)
  | `Null -> Error "cannot extract type from null"
  | `A _ -> Error "cannot extract type from array"
  | `O l ->
    let prim = List.find_map (function "prim", `String s -> Some s | _ -> None) l in
    match prim with
    | None -> Location.raise_errorf "cannot find prim name"
    | Some prim ->
      let args = Option.value ~default:(Ok []) @@ List.find_map (function
          | "args", `A l ->
            let l = List.map json l in
            begin match List.find_map (function Error e -> Some e | _ -> None) l with
              | Some e -> Some (Error e)
              | None -> Some (Ok (List.filter_map (function Error _ -> None | Ok x -> Some x) l))
            end
          | _ -> None) l in
      let annots = Option.value ~default:(Ok []) @@ List.find_map (function
          | "annots", `A l ->
            let l = List.map (function `String s -> Ok s | _ -> Error "annot is not string") l in
            begin match List.find_map (function Error e -> Some e | _ -> None) l with
              | Some e -> Some (Error e)
              | None -> Some (Ok (List.filter_map (function Error _ -> None | Ok x -> Some x) l))
            end
          | _ -> None) l in
      match args, annots with
      | Error e, _ | _, Error e -> Error e
      | Ok args, Ok annots -> Ok {prim; args; annots}

let micheline s =
  try Ok (parse_type s)
  with Failure s -> Error s

let types = [
  "address"; "bool"; "bytes"; "chain_id"; "contract"; "int"; "key"; "key_hash";
  "lambda"; "mutez"; "nat"; "operation"; "string"; "timestamp"; "unit"; "option";
  "list"; "set"; "or"; "map"; "big_map"; "pair"; "ticket"; "chest"; "chest_key";
  "bls12_381_fr"; "bls12_381_g1"; "bls12_381_g2"; "never"; "sapling_state";
  "sapling_transaction"; "sapling_transaction_deprecated"; "tx_rollup_l2_address" ]

let string s =
  let aux () =
    try match parse_type s with
      | { prim; args=[]; annots=[] } when not (List.mem prim types) -> Error (Format.sprintf "unknown primitive %S" s)
      | t -> Ok t
    with Failure s -> Error s in
  try match json (Ezjsonm.value_from_string s) with Ok t -> Ok t | Error _ -> aux ()
  with _ -> aux ()
