open Ppxlib
open Micheline_parser
open Ast_builder.Default

let lid ~loc s = Located.lident ~loc s

let patm ~loc s =
  ppat_construct ~loc (lid ~loc ("Proto.M" ^ s)) (Some (pvar ~loc "x"))
let pprim ~loc p =
  [%pat? Proto.Mprim { prim = [%p ppat_variant ~loc p None]; _ }]
let pprim_arg1 ~loc p =
  [%pat? Proto.Mprim { prim = [%p ppat_variant ~loc p None]; args = [ arg ]; _ }]
let read ~loc f =
  [%expr Result.map fst (
      [%e evar ~loc ("Tzfunc.Binary.Reader." ^ f)]
        {Tzfunc.Binary.Reader.s = Tzfunc.Crypto.hex_to_raw x; offset = 0})]

let ocase ~loc ?(rhs=[%expr Ok x]) s = case ~guard:None ~lhs:(patm ~loc s) ~rhs

let efunction ~loc ?(error="") cases =
  pexp_function ~loc @@
  cases @ [
    case ~guard:None ~lhs:(ppat_any ~loc) ~rhs:([%expr Error (`unexpected_michelson [%e estring ~loc error])]) ]

let str_or_bytes ~loc s =
  efunction ~loc ~error:"neither string or bytes" [ ocase ~loc "string"; ocase ~loc "bytes" ~rhs:(read ~loc s) ]

let rec resolve_extract ~loc x : expression =
  match x with
  | `s e -> e
  | `p l ->
    let no_names = List.for_all (fun (n, _) -> Option.is_none n) l in
    let rec aux i e = function
      | [_, f1; _, f2] ->
        let end_ =
          if no_names then
            [%expr Ok [%e pexp_tuple ~loc @@ List.mapi (fun i _ -> evar ~loc ("x" ^ string_of_int i)) l]]
          else
            [%expr Ok [%e
                pexp_object ~loc @@ class_structure ~self:(ppat_any ~loc)
                  ~fields:(List.mapi (fun i (n, _) ->
                      let txt = match n with None -> ("_" ^ string_of_int i) | Some n -> n in
                      pcf_method ~loc
                        ({txt; loc}, Public,
                         Cfk_concrete (Fresh, evar ~loc ("x" ^ string_of_int i)))) l)]] in
        let lhs1 = [%pat? (Proto.Mprim { prim=`Pair; args=[arg1; arg2]; _ } | Proto.Mseq [arg1; arg2])] in
        let lhs2 = [%pat? (Proto.Mprim { prim=`Pair; args=(arg1 :: args); _ } | Proto.Mseq (arg1 :: args))] in
        let rhs1 = pexp_match ~loc [%expr ([%e f1] arg1, [%e f2] arg2)] [
            case ~guard:None ~lhs:[%pat? (Ok [%p pvar ~loc ("x" ^ string_of_int i)],
                                          Ok [%p pvar ~loc ("x" ^ string_of_int (i+1))])]
              ~rhs:end_;
            case ~guard:None ~lhs:[%pat? Error _e, _ | _, Error _e] ~rhs:[%expr Error _e]
          ] in
        let rhs2 = pexp_match ~loc [%expr ([%e f1] arg1, [%e f2] (Proto.Mseq args))] [
            case ~guard:None ~lhs:[%pat? (Ok [%p pvar ~loc ("x" ^ string_of_int i)],
                                          Ok [%p pvar ~loc ("x" ^ string_of_int (i+1))])]
              ~rhs:end_;
            case ~guard:None ~lhs:[%pat? Error _e, _ | _, Error _e] ~rhs:[%expr Error _e]
          ] in
        pexp_match ~loc e [
          case ~guard:None ~lhs:lhs1 ~rhs:rhs1;
          case ~guard:None ~lhs:lhs2 ~rhs:rhs2;
          case ~guard:None ~lhs:[%pat? _v] ~rhs:[%expr Error (`unexpected_michelson "neither a pair or seq of 2 arguments")
          ]
        ]
      | (_, f) :: t ->
        let lhs1 = [%pat? (Proto.Mprim { prim=`Pair; args=[arg1; arg2]; _ } | Proto.Mseq [arg1; arg2])] in
        let lhs2 = [%pat? (Proto.Mprim { prim=`Pair; args=(arg1 :: args); _ } | Proto.Mseq (arg1 :: args))] in
        let rhs1 = pexp_match ~loc [%expr [%e f] arg1] [
            case ~guard:None ~lhs:[%pat? Ok [%p pvar ~loc ("x" ^ string_of_int i)]]
              ~rhs:(aux (i+1) [%expr arg2] t);
            case ~guard:None ~lhs:[%pat? Error e] ~rhs:[%expr Error e]
          ] in
        let rhs2 = pexp_match ~loc [%expr [%e f] arg1] [
            case ~guard:None ~lhs:[%pat? Ok [%p pvar ~loc ("x" ^ string_of_int i)]]
              ~rhs:(aux (i+1) [%expr Proto.Mseq args] t);
            case ~guard:None ~lhs:[%pat? Error e] ~rhs:[%expr Error e]
          ] in
        pexp_match ~loc e [
          case ~guard:None ~lhs:lhs1 ~rhs:rhs1;
          case ~guard:None ~lhs:lhs2 ~rhs:rhs2;
          case ~guard:None ~lhs:(ppat_any ~loc) ~rhs:[%expr Error (`unexpected_michelson "neither a pair or seq")]
        ]
      | _ -> Location.raise_errorf "pair with less than 2 arguments"
    in
    [%expr fun m -> [%e aux 0 [%expr m] l]]
  | `o (l, r) ->
    let lhs_l = [%pat? (Proto.Mprim { prim=`Left; args=[arg]; _ })] in
    let lhs_r = [%pat? (Proto.Mprim { prim=`Right; args=[arg]; _ })] in
    let aux (n, index, x) =
      let f = resolve_extract ~loc x in
      match n, x with
      | _, `s _ | _, `p _ | Some _, _ ->
        let n = Option.value ~default:(string_of_int index) n in
        [%expr Result.map (fun x -> [%e pexp_variant ~loc n (Some [%expr x])])
            ([%e f] arg)]
      | None, `o _ -> [%expr [%e f] arg] in
    pexp_function ~loc [
      case ~guard:None ~lhs:lhs_l ~rhs:(aux l);
      case ~guard:None ~lhs:lhs_r ~rhs:(aux r);
      case ~guard:None ~lhs:(ppat_any ~loc) ~rhs:[%expr Error (`unexpected_michelson "neither left or right")]
    ]

let rec extract_aux ~loc ?(index=0) t =
  let name =
    try let s = List.hd t.annots in Some (String.sub s 1 (String.length s - 1))
    with _ -> None in
  let e, index = match t.prim, t.args with
    | "address", [] -> `s (str_or_bytes ~loc "contract"), index
    | "bool", [] ->
      `s (
        efunction ~loc ~error:"neither false or true for bool" [
          case ~guard:None ~lhs:(pprim ~loc "True") ~rhs:[%expr Ok true];
          case ~guard:None ~lhs:(pprim ~loc "False") ~rhs:[%expr Ok false]
        ]), index
    | ("bytes" | "chest" | "chest_key" | "bls12_381_g1" | "bls12_381_g2"), [] ->
      `s (efunction ~loc ~error:"not bytes" [ ocase ~loc "bytes" ]), index
    | "chain_id", [] -> `s (str_or_bytes ~loc "chain_id"), index
    | ("int" | "mutez" | "nat"), [] -> `s (efunction ~error:"not int" ~loc [ ocase ~loc "int" ]), index
    | "bls12_381_fr", [] ->
      `s (efunction ~error:"neither int or bytes" ~loc [
          ocase ~loc "int";
          ocase ~loc "bytes" ~rhs:[%expr Ok (Z.of_string ("0x" ^ (x :> string)))]
        ]), index
    | "key", [] -> `s (str_or_bytes ~loc "pk"), index
    | "key_hash", [] -> `s (str_or_bytes ~loc "pkh"), index
    | "string", [] -> `s (efunction ~loc ~error:"not string" [ ocase ~loc "string" ]), index
    | "tx_rollup_l2_address", [] -> `s (str_or_bytes ~loc "l2_address"), index
    | "timestamp", [] ->
      `s (
        efunction ~loc ~error:"neither int, string or bytes for timestamp" [
          ocase ~loc "int" ~rhs:[%expr
            try Ok (CalendarLib.Calendar.from_unixfloat (Z.to_float x))
            with exn -> Error (`generic ("timestamp_error", Printexc.to_string exn))];
          ocase ~loc "string" ~rhs:[%expr
            try Ok (Proto.A.cal_of_str x)
            with exn -> Error (`generic ("timestamp_error", Printexc.to_string exn))];
          ocase ~loc "bytes" ~rhs:[%expr
            try Ok (Proto.A.cal_of_str (Tzfunc.Crypto.hex_to_raw x :> string))
            with exn -> Error (`generic ("timestamp_error", Printexc.to_string exn))];
        ]), index
    | "unit", [] ->
      `s (efunction ~loc ~error:"not unit" [ case ~guard:None ~lhs:(pprim ~loc "Unit") ~rhs:[%expr Ok ()] ]), index
    | "option", [ t ] ->
      let _, f, _ = extract_aux ~loc t in
      let f = resolve_extract ~loc f in
      `s (
        efunction ~loc ~error:"neiter none or some" [
          case ~guard:None ~lhs:(pprim ~loc "None") ~rhs:[%expr Ok None];
          case ~guard:None ~lhs:(pprim_arg1 ~loc "Some")
            ~rhs:[%expr Result.map Option.some ([%e f] arg)]
        ]), index
    | "list", [ t ] | "set", [ t ] ->
      let _, f, _ = extract_aux ~loc t in
      let f = resolve_extract ~loc f in
      `s (
        efunction ~loc ~error:"not seq for list or set" [
          case ~guard:None ~lhs:[%pat? Proto.Mseq l]
            ~rhs:[%expr
              let rec aux acc = function
                | [] -> Ok (List.rev acc)
                | h :: t -> match [%e f] h with
                  | Error e -> Error e
                  | Ok x -> aux (x :: acc) t in
              aux [] l]
        ]), index
    | "or", [ tl; tr ] ->
      let nl, fl, index_l = extract_aux ~loc ~index tl in
      let index_l = match nl with None -> index_l | Some _ -> 1 in
      let nr, fr, index_r = extract_aux ~loc ~index:index_l tr in
      `o ((nl, index, fl),
          (nr, index_l, fr)), index_r - 1
    | "map", [ tk; tv ] ->
      let _, fk, _ = extract_aux ~loc tk in
      let fk = resolve_extract ~loc fk in
      let _, fv, _ = extract_aux ~loc tv in
      let fv = resolve_extract ~loc fv in
      `s (
        efunction ~loc ~error:"not a seq for a map" [
          case ~guard:None ~lhs:[%pat? Proto.Mseq l]
            ~rhs:[%expr
              let rec aux acc = function
                | [] -> Ok (List.rev acc)
                | Proto.Mprim { prim = `Elt; args = [k; v]; _ } :: t ->
                  begin match [%e fk] k, [%e fv] v with
                    | Error e, _ | _, Error e -> Error e
                    | Ok k, Ok v -> aux ((k, v) :: acc) t
                  end
                | _ -> Error (`unexpected_michelson "not an elt for a map") in
              aux [] l]
        ]), index
    | "big_map", [ tk; tv ] ->
      let _, fk, _ = extract_aux ~loc tk in
      let fk = resolve_extract ~loc fk in
      let _, fv, _ = extract_aux ~loc tv in
      let fv = resolve_extract ~loc fv in
      `s (
        efunction ~loc ~error:"neither a seq or and id for a big_map" [
          case ~guard:None ~lhs:[%pat? Proto.Mseq l]
            ~rhs:[%expr
              let rec aux acc = function
                | [] -> Ok (`list (List.rev acc))
                | Proto.Mprim { prim = `Elt; args = [k; v]; _ } :: t ->
                  begin match [%e fk] k, [%e fv] v with
                    | Error e, _ | _, Error e -> Error e
                    | Ok k, Ok v -> aux ((k, v) :: acc) t
                  end
                | _ -> Error (`unexpected_michelson "not an elt for a big map") in
              aux [] l];
          case ~guard:None ~lhs:[%pat? Proto.Mint i] ~rhs:[%expr Ok (`id i)]
        ]), index
    | "pair", [ t1; t2 ] ->
      let name1, f1, _ = extract_aux ~loc t1 in
      let f1 = resolve_extract ~loc f1 in
      begin match extract_aux ~loc t2 with
        | (Some _) as name2, f2, _ -> `p [ name1, f1; name2, resolve_extract ~loc f2 ]
        | _name2, `p l, _ -> `p ((name1, f1) :: l)
        | name2, f2, _ -> `p [ name1, f1; name2, resolve_extract ~loc f2 ]
      end, index
    | "pair", (t1 :: args) ->
      let _, f, _ = extract_aux ~loc {prim="pair"; args=[t1; {prim="pair"; args; annots=[]}]; annots=t.annots} in
      f, index
    | ("operation" | "contract" | "lambda" | "ticket" | "sapling_state"
      | "sapling_transaction" | "sapling_transaction_deprecated"), _ ->
      `s [%expr fun m -> Ok m], index
    | _ -> Location.raise_errorf "primitive type not handled %S(%d)" t.prim (List.length t.args) in
  name, e, index + 1

let extract ~loc t =
  let _, f, _ = extract_aux ~loc t in
  resolve_extract ~loc f
