open Ppxlib
open Micheline_parser
open Ast_builder.Default

let resolve_produce ~loc = function
  | `s e -> e
  | `p l ->
    let no_names = List.for_all (fun (n, _) -> Option.is_none n) l in
    if no_names then
      [%expr fun [%p ppat_tuple ~loc @@ List.mapi (fun i _ -> pvar ~loc ("x" ^ string_of_int i)) l ] ->
        Proto.Mseq [%e elist ~loc @@ List.mapi (fun i (_, f) -> eapply ~loc f [ evar ~loc ("x" ^ string_of_int i) ]) l]]
    else
      [%expr fun o ->
        Proto.Mseq [%e elist ~loc @@ List.mapi (fun i (n, f) ->
            let txt = match n with None -> "_" ^ (string_of_int i) | Some n -> n in
            eapply ~loc f [ pexp_send ~loc [%expr o] {txt; loc} ]) l]]
  | `o l ->
    let rec aux f = function
      | [] -> [%expr [%e f] x]
      | `l :: t -> [%expr Proto.prim `Left ~args:[[%e aux f t ]]]
      | `r :: t -> [%expr Proto.prim `Right ~args:[[%e aux f t ]]] in
    pexp_function ~loc @@ List.map (fun (n, f, d) ->
        let lhs = ppat_variant ~loc n (Some [%pat? x]) in
        let rhs = aux f d in
        case ~guard:None ~lhs ~rhs) l

let rec produce_aux ~loc ?(index=0) t =
  let name =
    try let s = List.hd t.annots in Some (String.sub s 1 (String.length s - 1))
    with _ -> None in
  let e, index = match t.prim, t.args with
    | ("address" | "chain_id" | "key" | "key_hash" | "string" | "tx_rollup_l2_address"), [] ->
      `s [%expr fun s -> Proto.Mstring s], index
    | "bool", [] -> `s [%expr fun b -> if b then Proto.prim `True else Proto.prim `False], index
    | ("bytes" | "chest" | "chest_key" | "bls12_381_g1" | "bls12_381_g2"), [] ->
      `s [%expr fun s -> Proto.Mbytes s], index
    | ("int" | "mutez" | "nat" | "bls12_381_fr"), [] ->
      `s [%expr fun i -> Proto.Mint i], index
    | "timestamp", [] -> `s [%expr fun d -> Proto.Mstring (Proto.A.cal_to_str d)], index
    | "unit", [] -> `s [%expr fun () -> Proto.prim `Unit], index
    | "option", [ t ] ->
      let _, f, _ = produce_aux ~loc t in
      let f = resolve_produce ~loc f in
      `s [%expr function
        | None -> Proto.prim `None
        | Some x -> Proto.prim `Some ~args:[[%e f] x]], index
    | "list", [ t ] ->
      let _, f, _ = produce_aux ~loc t in
      let f = resolve_produce ~loc f in
      `s [%expr fun l -> Proto.Mseq (List.map [%e f] l)], index
    | "set", [ t ] ->
      let _, f, _ = produce_aux ~loc t in
      let f = resolve_produce ~loc f in
      `s [%expr fun l -> Proto.Mseq (List.map [%e f] (List.sort compare l))], index
    | "or", [ tl; tr ] ->
      let nl, fl, index_l = produce_aux ~loc ~index tl in
      let index_l = match nl with None -> index_l | Some _ -> 1 in
      let nr, fr, index_r = produce_aux ~loc ~index:index_l tr in
      let fl_res = resolve_produce ~loc fl in
      let fr_res = resolve_produce ~loc fr in
      let x = match nl, fl, nr, fr with
        | None, `o ll, None, `o lr ->
          let ll = List.map (fun (n, f, d) -> (n, f, `l :: d)) ll in
          let lr = List.map (fun (n, f, d) -> (n, f, `r :: d)) lr in
          `o (ll @ lr)
        | None, `o ll, _, _ ->
          let ll = List.map (fun (n, f, d) -> (n, f, `l :: d)) ll in
          `o (ll @ [ Option.value ~default:(string_of_int index_l) nr, fr_res, [ `r ] ])
        | _, _, None, `o lr ->
          let lr = List.map (fun (n, f, d) -> (n, f, `r :: d)) lr in
          `o ((Option.value ~default:(string_of_int index) nl, fl_res, [ `l ]) :: lr)
        | _ ->
          `o [ Option.value ~default:(string_of_int index) nl, fl_res, [ `l ];
               Option.value ~default:(string_of_int index_l) nr, fr_res, [ `r ] ] in
      x, index_r - 1
    | "map", [ tk; tv ] ->
      let _, fk, _ = produce_aux ~loc tk in
      let _, fv, _ = produce_aux ~loc tv in
      let fk = resolve_produce ~loc fk in
      let fv = resolve_produce ~loc fv in
      `s [%expr fun l -> Proto.Mseq (List.map (fun (k, v) ->
          Proto.prim `Elt ~args:[[%e fk] k; [%e fv] v])
          (List.sort (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) l))], index
    | "big_map", [ tk; tv ] ->
      let _, fk, _ = produce_aux ~loc tk in
      let _, fv, _ = produce_aux ~loc tv in
      let fk = resolve_produce ~loc fk in
      let fv = resolve_produce ~loc fv in
      `s [%expr fun (`list l) ->
        Proto.Mseq (List.map (fun (k, v) ->
            Proto.prim `Elt ~args:[[%e fk] k; [%e fv] v]) (List.sort Stdlib.compare l))],
      index
    | "pair", [ t1; t2 ] ->
      let name1, f1, _ = produce_aux ~loc t1 in
      let f1 = resolve_produce ~loc f1 in
      begin match produce_aux ~loc t2 with
        | (Some _) as name2, f2, _ -> `p [ name1, f1; name2, resolve_produce ~loc f2 ], index
        | _name2, `p l, _ -> `p ((name1, f1) :: l), index
        | name2, f2, _ -> `p [ name1, f1; name2, resolve_produce ~loc f2 ], index
      end
    | "pair", (t1 :: args) ->
      let _, f, _ = produce_aux ~loc {prim="pair"; args=[t1; {prim="pair"; args; annots=[]}]; annots=t.annots} in
      f, index
    | ("operation" | "contract" | "lambda" | "ticket" | "sapling_state"
      | "sapling_transaction" | "sapling_transaction_deprecated"), _ ->
      `s [%expr fun m -> m], index
    | _ -> Location.raise_errorf "primitive type not handled %S(%d)" t.prim (List.length t.args) in
  name, e, index + 1

let produce ~loc t =
  let _, f, _ = produce_aux ~loc t in
  resolve_produce ~loc f
