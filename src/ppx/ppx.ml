open Ppxlib
open Micheline_parser
open Ast_builder.Default

type options = {
  node: string;
  contract: string option;
  storage: micheline_type option;
  entrypoint_names: string list option;
  entrypoints: (string * micheline_type) list option;
  views: (string * micheline_type * micheline_type) list option;
  view_names: string list option;
  file: string option;
  debug: bool;
  no_storage: bool;
  code: string option;
  encodings: bool;
  bigmaps: (string * (micheline_type * string) option * micheline_type option) list option;
}

let dir_path = ref (Sys.getenv "PWD")

let node = ref "https://tz.functori.com"
let () = match Sys.getenv_opt "MTYPED_NODE" with
  | Some s -> node := s
  | _ -> ()

let rec get_string_list acc e = match e.pexp_desc with
  | Pexp_construct ({txt=Lident "[]"; _}, None) -> Some (List.rev acc)
  | Pexp_construct ({txt=Lident "::"; _}, Some {pexp_desc=Pexp_tuple [
      {pexp_desc=Pexp_constant (Pconst_string (s, _, _)); _};
      e
    ]; _}) -> get_string_list (s :: acc) e
  | _ -> None

let get_url url =
  try
    let r = Buffer.create 16384 in
    let c = Curl.init () in
    Curl.set_sslverifypeer c false;
    Curl.set_sslverifyhost c Curl.SSLVERIFYHOST_EXISTENCE;
    Curl.set_writefunction c (fun s -> Buffer.add_string r s; String.length s);
    Curl.set_verbose c false;
    Curl.set_customrequest c "GET";
    Curl.set_followlocation c true;
    Curl.set_url c url;
    Curl.perform c;
    let rc = Curl.get_responsecode c in
    Curl.cleanup c;
    if rc >= 200 && rc < 300 then Buffer.contents r
    else Location.raise_errorf "cannot fetch url %S" url
  with exn ->
    Location.raise_errorf "curl error for %S:\n%s" url (Printexc.to_string exn)

let rec micheline_type_to_json t =
  let args = List.map micheline_type_to_json t.args in
  let annots = List.map (fun s -> `String s) t.annots in
  `O [ "prim", `String t.prim; "args", `A args; "annots", `A annots ]


let get_json_code : Ezjsonm.value -> Ezjsonm.value option = function
  | `O l -> List.find_map (function ("code", json) -> Some json | _ -> None) l
  | _ -> None

let get_code_elt name : Ezjsonm.value -> Ezjsonm.value option = function
  | `A l ->
    List.find_map (function
        | `O l ->
          if List.exists (function ("prim", `String s) when s = name -> true | _ -> false) l then (
            List.find_map (function ("args", `A [ json ]) -> Some json | _ -> None) l)
          else None
        | _ -> None) l
  | _ -> None

let get_code_view v : Ezjsonm.value -> (Ezjsonm.value * Ezjsonm.value) option = function
  | `A l ->
    List.find_map (function
        | `O l ->
          if List.exists (function ("prim", `String "view") -> true | _ -> false) l then
            List.find_map (function ("args", `A [ `O ["string", `String n]; i; o; _ ]) when n = v -> Some (i, o) | _ -> None) l
          else None
        | _ -> None) l
  | _ -> None

let get_views = function
  | `A l ->
    Some (List.filter_map (function
        | `O l ->
          if List.exists (function ("prim", `String "view") -> true | _ -> false) l then
            List.find_map (function ("args", `A [ `O ["string", `String n]; i; o; _ ]) -> Some (n, i, o) | _ -> None) l
          else None
        | _ -> None) l)
  | _ -> None

let get_code_entrypoint e (json : Ezjsonm.value) : Ezjsonm.value option =
  let rec aux : Ezjsonm.value -> Ezjsonm.value option = function
    | `O l ->
      if List.exists (function ("annots", `A (`String s :: _)) when s = "%" ^ e -> true | _ -> false) l then
        Some (`O l)
      else if not (List.exists (function ("prim", `String "or") -> true | _ -> false) l) then None
      else
        begin match List.find_map (function ("args", `A [ l; r ]) -> Some (l, r) | _ -> None) l with
          | None -> None
          | Some (l, r) ->
            match aux l with
            | Some x -> Some x
            | None -> aux r
        end
    | _ -> None in
  Option.bind (get_code_elt "parameter" json) aux

let get_entrypoints (json : Ezjsonm.value) : (string * Ezjsonm.value) list option =
  let rec aux acc = function
    | `O l ->
      let acc = match List.find_map (function ("annots", `A (`String s :: _)) when s.[0] = '%' -> Some s | _ -> None) l with
        | Some n -> (String.sub n 1 (String.length n - 1), `O l) :: acc
        | None -> acc in
      if not (List.exists (function ("prim", `String "or") -> true | _ -> false) l) then
        List.rev acc
      else
        begin match List.find_map (function ("args", `A [ l; r ]) -> Some (l, r) | _ -> None) l with
          | None -> List.rev acc
          | Some (l, r) ->
            let acc = aux acc l in
            aux acc r
        end
    | _ -> List.rev acc in
  Option.map (aux []) (get_code_elt "parameter" json)

let get_bigmaps t =
  let i = ref 0 in
  let rec aux acc t = match t.prim, t.args with
    | "big_map", [ tk; tv ] ->
      let name =
        try let s = List.hd t.annots in String.sub s 1 (String.length s - 1)
        with _ -> "bigmap" ^ string_of_int !i in
      incr i;
      let typ = Ezjsonm.value_to_string @@ micheline_type_to_json tk in
      (name, Some (tk, typ), Some tv) :: acc
    | "pair", _ -> List.fold_left aux acc t.args
    | _ -> acc in
  aux [] t

let string_or_env s =
  let n = String.length s in
  if n < 2 then Some s
  else if not (String.get s 0 = '$') then Some s
  else
    let env = if String.get s 1 = '{' then String.trim (String.sub s 2 (n-3)) else String.sub s 1 (n-1) in
    let env, dft = match String.index_opt env ':' with
      | None -> env, None
      | Some i ->
        String.trim (String.sub env 0 i),
        Some (String.trim (String.sub env (i+1) (String.length env -i-1))) in
    match Sys.getenv_opt env, dft with
    | (None | Some ""), Some dft -> Some dft
    | None, None -> Format.eprintf "environment variable %S not found@." s; None
    | Some s, _ -> Some s

let get_options ~loc e =
  let parse_string ~error f s =
    match Tzfunc_ppx.Parse.string s with
    | Ok t -> f t
    | Error s -> error s in
  let options = match e.pexp_desc with
    | Pexp_record (l, None) ->
      let o = {
        node = !node; contract = None; storage = None; entrypoint_names = None;
        entrypoints = None; views = None; view_names = None;
        debug = false; no_storage = false; file = None; code = None; bigmaps = None;
        encodings = true} in
      List.fold_left (fun o ({txt; _}, e) ->
          let id = Longident.name txt in
          match id, e.pexp_desc with
          | "node", Pexp_constant (Pconst_string (node, _, _)) -> {o with node}
          | "encodings", Pexp_construct ({txt=Lident "false"; _}, None) ->
            { o with encodings = false }
          | "network", Pexp_constant (Pconst_string (s, _, _)) ->
            {o with node = Format.sprintf "https://%s.tz.functori.com" s}
          | "contract", Pexp_constant (Pconst_string (s, _, _)) ->
            { o with contract = Some s }
          | "contract", (Pexp_construct ({txt=Lident s; _}, None) | Pexp_ident {txt=Lident s; _}) ->
            begin match Sys.getenv_opt s with
              | None -> Format.eprintf "environment variable %S not found@." s; o
              | Some s -> { o with contract = Some s }
            end
          | "file", Pexp_constant (Pconst_string (s, _, _)) ->
            begin match string_or_env s with
              | None -> o
              | Some s ->
                let s =
                  if Filename.is_relative s then Filename.concat !dir_path s
                  else s in
                { o with file = Some s }
            end
          | "file", (Pexp_construct ({txt=Lident s; _}, None) | Pexp_ident {txt=Lident s; _}) ->
            begin match Sys.getenv_opt s with
              | None -> Format.eprintf "environment variable %S not found@." s; o
              | Some s ->
                let s =
                  if Filename.is_relative s then Filename.concat !dir_path s
                  else s in
                { o with file = Some s }
            end
          | "storage", Pexp_constant (Pconst_string (s, _, _)) ->
            let error s = Location.raise_errorf ~loc "cannot parse michelson type:\n%s" s in
            {o with storage = Some (parse_string ~error (fun x -> x) s)}
          | "entrypoint", Pexp_constant (Pconst_string (s, _, _)) ->
            let error s = Some [s], None in
            let f s = None, Some ["entrypoint", s] in
            let entrypoint_names, entrypoints = parse_string ~error f s in
            {o with entrypoint_names; entrypoints}
          | "entrypoints", _ ->
            begin match get_string_list [] e with
              | None -> o
              | Some l -> { o with entrypoint_names = Some l }
            end
          | "view", Pexp_constant (Pconst_string (s, _, _)) ->
            { o with view_names = Some [ s ]}
          | "views", _ ->
            begin match get_string_list [] e with
              | None -> o
              | Some l -> { o with view_names = Some l }
            end
          | "view_input", Pexp_constant (Pconst_string (s, _, _)) ->
            let error s = Location.raise_errorf "cannot parse michelson type:\n%s" s in
            let views = match o.views with
              | None -> Some [parse_string ~error (fun x -> ("view", x, {prim=""; args=[]; annots=[]})) s]
              | Some [v, _, o] -> Some [parse_string ~error (fun x -> (v, x, o)) s]
              | _ -> Location.raise_errorf "several view_input" in
            { o with views }
          | "view_output", Pexp_constant (Pconst_string (s, _, _)) ->
            let error s = Location.raise_errorf "cannot parse michelson type:\n%s" s in
            let views = match o.views with
              | None -> Some [parse_string ~error (fun x -> ("view", {prim=""; args=[]; annots=[]}, x)) s]
              | Some [v, i, _] -> Some [parse_string ~error (fun x -> (v, i, x)) s]
              | _ -> Location.raise_errorf "several view_input" in
            { o with views }
          | "key", Pexp_constant (Pconst_string (s, _, _)) ->
            let error s = Location.raise_errorf "cannot parse michelson type:\n%s" s in
            let bigmaps = match o.bigmaps with
              | None -> Some [parse_string ~error (fun x ->
                  ("bigmap", Some (x, Ezjsonm.value_to_string (micheline_type_to_json x)), None)) s]
              | Some [v, _, o] -> Some [parse_string ~error (fun x -> (v, Some (x, Ezjsonm.value_to_string (micheline_type_to_json x)), o)) s]
              | _ -> Location.raise_errorf "several view_input" in
            { o with bigmaps }
          | "value", Pexp_constant (Pconst_string (s, _, _)) ->
            let error s = Location.raise_errorf "cannot parse michelson type:\n%s" s in
            let bigmaps = match o.bigmaps with
              | None -> Some [parse_string ~error (fun x -> ("bigmap", None, Some x)) s]
              | Some [v, i, _] -> Some [parse_string ~error (fun x -> (v, i, Some x)) s]
              | _ -> Location.raise_errorf "several view_input" in
            { o with bigmaps }
          | "bigmaps", _ ->
            begin match get_string_list [] e with
              | None -> o
              | Some l -> { o with bigmaps = Some (List.map (fun s -> s, None, None) l) }
            end
          | "debug", _ -> {o with debug=true}
          | "no_storage", _ -> {o with no_storage=true}
          | s, _ -> Format.eprintf "option %S not recognized" s; o) o l
    | _ -> Location.raise_errorf "options need to be a record" in
  let code = match options.contract, options.file with
    | None, None -> None
    | None, Some f ->
      let ic = open_in f in
      let s = really_input_string ic (in_channel_length ic) in
      close_in ic;
      if Filename.extension f = ".json" then Some (Ezjsonm.value_from_string s)
      else Some (parse s)
    | Some c, _ ->
      get_json_code @@ Ezjsonm.value_from_string @@ get_url @@
      Format.sprintf "%s/chains/main/blocks/head/context/contracts/%s/script"
        options.node c in
  match code with
  | None -> options
  | Some json ->
    let code, storage, bigmaps =
      let storage, bigmaps =
        match Option.map Tzfunc_ppx.Parse.json @@ get_code_elt "storage" json with
        | None -> None, None
        | Some (Error s) -> Location.raise_errorf ~loc "%s" s
        | Some (Ok t) ->
          let bigmaps = match options.bigmaps with
            | None -> List.rev @@ get_bigmaps t
            | Some [] -> []
            | Some l ->
              let bms = List.rev @@ get_bigmaps t in
              List.filter (fun (n, _, _) -> List.exists (fun (n2, _, _) -> n2 = n) l) bms in
          Some t, Some bigmaps in
      if options.no_storage then None, None, bigmaps
      else
        Some (Ezjsonm.value_to_string json), storage, bigmaps
    in
    let entrypoints = match options.entrypoint_names, options.entrypoints with
      | None, None ->
        Option.map (List.map (fun (n, x) -> match Tzfunc_ppx.Parse.json x with
            | Error s -> Location.raise_errorf ~loc "%s" s
            | Ok t -> n, t)) @@ get_entrypoints json
      | None, Some l -> Some l
      | Some l, _ ->
        Some (List.filter_map (fun e ->
            Option.map (fun x -> match Tzfunc_ppx.Parse.json x with
                | Error s -> Location.raise_errorf ~loc "%s" s
                | Ok t -> e, t) @@ get_code_entrypoint e json) l) in
    let views = match options.view_names, options.views with
      | None, None ->
        Option.map (List.map (fun (n, i, o) ->
            match Tzfunc_ppx.Parse.json i, Tzfunc_ppx.Parse.json o with
            | Error e, _ | _, Error e -> Location.raise_errorf ~loc "%s" e
            | Ok i, Ok o -> n, i, o)) @@
        get_views json
      | None, Some l -> Some l
      | Some l, _ ->
        Some (List.filter_map (fun v ->
            Option.map (fun (i, o) ->
                match Tzfunc_ppx.Parse.json i, Tzfunc_ppx.Parse.json o with
                | Error e, _ | _, Error e -> Location.raise_errorf ~loc "%s" e
                | Ok i, Ok o -> v, i, o) @@
            get_code_view v json) l) in
    {options with code; storage; entrypoints; views; bigmaps}

let storage ~loc ~name ?(item=false) options =
  let e =
    if not item then pexp_send ~loc [%expr self] {txt=name; loc}
    else evar ~loc name in
  let e = [%expr
    Lwt.bind (Tzfunc.Node.get_storage ?base ?block contract) @@ function
    | Error e -> Lwt.return_error e
    | Ok s -> Lwt.return ([%e e] s)] in
  match options.contract, options.storage with
  | None, Some _ ->
    Some [%expr fun ?base ?block contract -> [%e e]]
  | Some c, Some _ ->
    Some [%expr fun ?base ?block () ->
      let contract = [%e estring ~loc c] in
      [%e e]]
  | _ -> None

let deploy  ~loc ~name ?(item=false) options =
  let e =
    if not item then pexp_send ~loc [%expr self] {txt=name; loc}
    else evar ~loc name in
  let e = [%expr
    let storage = [%e e] storage in
    match Tzfunc.Crypto.Sk.b58dec edsk with
    | Error e -> Lwt.return_error e
    | Ok sk ->
      let pk = Tzfunc.Crypto.Sk.to_public_key sk in
      let get_pk () = Lwt.return_ok (Tzfunc.Crypto.Pk.b58enc pk) in
      let source = Tzfunc.Crypto.(Pkh.b58enc @@ Pk.hash pk) in
      let sign = Tzfunc.Node.sign ~edsk in
      let ori = Tzfunc.Utils.origination ?fee ?counter ?gas_limit ?storage_limit ?balance
          ~code ~source (Micheline storage) in
      Tzfunc.Utils.send ?base ~get_pk ?remove_failed ?forge_method ~sign [ ori ]] in
  match options.code, options.storage, options.no_storage with
  | _, _, true | _, None, _ -> None
  | Some c, _, _ ->
    Some
      [%expr fun ?base ?fee ?counter ?gas_limit ?storage_limit ?balance ?remove_failed
        ?forge_method ~edsk storage ->
        let code = EzEncoding.destruct Proto.script_expr_enc.Proto.json [%e estring ~loc c] in
        [%e e]]
  | None, _, _ ->
    Some
      [%expr fun ?base ?fee ?counter ?gas_limit ?storage_limit ?balance ?remove_failed
        ?forge_method ~edsk ~code storage -> [%e e]]

let call ~loc ?(item=false) options (name, suffix, _, _, _) =
  let produce_name = if suffix = "" then "produce" else "produce_" ^ suffix in
  let e =
    if not item then pexp_send ~loc [%expr self] {txt=produce_name; loc}
    else evar ~loc produce_name in
  let e = [%expr
    let p = [%e e] p in
    match Tzfunc.Crypto.Sk.b58dec edsk with
    | Error e -> Lwt.return_error e
    | Ok sk ->
      let pk = Tzfunc.Crypto.Sk.to_public_key sk in
      let get_pk () = Lwt.return_ok (Tzfunc.Crypto.Pk.b58enc pk) in
      let source = Tzfunc.Crypto.(Pkh.b58enc @@ Pk.hash pk) in
      let sign = Tzfunc.Node.sign ~edsk in
      let tr = Tzfunc.Utils.transaction ?fee ?counter ?gas_limit ?storage_limit ?amount
          ~entrypoint ~param:(Micheline p) ~source contract in
      Tzfunc.Utils.send ?base ~get_pk ?remove_failed ?forge_method ~sign [ tr ]] in
  let e = match options.contract, name with
    | Some c, _ ->
      Some
        [%expr fun ?base ?fee ?counter ?gas_limit ?storage_limit ?amount ?remove_failed
          ?forge_method ~edsk p ->
          let contract = [%e estring ~loc c] in
          let entrypoint = [%e estring ~loc name] in
          [%e e]]
    | None, "entrypoint" ->
      Some
        [%expr fun ?base ?fee ?counter ?gas_limit ?storage_limit ?amount ?remove_failed
          ?forge_method ~edsk ~contract ~entrypoint p -> [%e e]]
    | None, _ ->
      Some
        [%expr fun ?base ?fee ?counter ?gas_limit ?storage_limit ?amount ?remove_failed
          ?forge_method ~edsk ~contract p ->
          let entrypoint = [%e estring ~loc name] in
          [%e e]] in
  Option.map (fun e -> (if suffix = "" then name else if name = "entrypoint" then "call" else "call_" ^ name), e) e

let view ~loc ?(item=false) options (name, suffix, _, _, _, _) =
  let extract_name, produce_name =
    if suffix = "" then "extract", "produce"
    else "extract_" ^ suffix, "produce_" ^ suffix in
  let e_extract, e_produce =
    if not item then
      pexp_send ~loc [%expr self] {txt=extract_name; loc},
      pexp_send ~loc [%expr self] {txt=produce_name; loc}
    else evar ~loc extract_name, evar ~loc produce_name in
  let e = [%expr
    let input = [%e e_produce] input in
    Lwt.bind (Tzfunc.Utils.view ?chain_id ?base ?amount ?balance ?source ?payer
                ?now ?level ~input ~contract name) @@ function
    | Error e -> Lwt.return_error e
    | Ok output -> Lwt.return ([%e e_extract] output)] in
  let e = match options.contract, name with
    | Some c, _ ->
      Some [%expr fun ?chain_id ?base ?amount ?balance ?source ?payer ?now ?level input ->
        let contract = [%e estring ~loc c] in
        let name = [%e estring ~loc name] in
        [%e e]]
    | None, "view" ->
      Some [%expr fun ?chain_id ?base ?amount ?balance ?source ?payer ?now ?level ~contract ~name input ->
        [%e e]]
    | None, _ ->
      Some [%expr fun ?chain_id ?base ?amount ?balance ?source ?payer ?now ?level ~contract input ->
        let name = [%e estring ~loc name] in
        [%e e]] in
  Option.map (fun e ->
      (if suffix = "" then name
       else if name = "view" then "view"
       else "view_" ^ name), e) e

let get_bigmap ~loc ?(item=false) name typ =
  let extract_name, produce_name =
    if name = "" then "extract_value", "produce_key"
    else "extract_" ^ name ^ "_value", "produce_" ^ name ^ "_key" in
  let e_extract, e_produce =
    if not item then
      pexp_send ~loc [%expr self] {txt=extract_name; loc},
      pexp_send ~loc [%expr self] {txt=produce_name; loc}
    else evar ~loc extract_name, evar ~loc produce_name in
  [%expr
    fun ?base ?block ~id key ->
      let typ = EzEncoding.destruct Proto.micheline_enc.Proto.json [%e estring ~loc typ] in
      let key = [%e e_produce] key in
      Lwt.bind (Tzfunc.Node.get_bigmap_value ?base ?block ~typ id key) @@ function
      | Error e -> Lwt.return_error e
      | Ok None -> Lwt.return_ok None
      | Ok (Some output) -> match [%e e_extract] output with
        | Ok x -> Lwt.return_ok (Some x)
        | Error e -> Lwt.return_error e]

let process ~loc ?item e =
  let options = get_options ~loc e in
  let extract_storage = Option.map (Tzfunc_ppx.extract ~loc) options.storage in
  let produce_storage = Option.map (Tzfunc_ppx.produce ~loc) options.storage in
  let storage_type = Option.map (Tzfunc_ppx.otype ~loc) options.storage in
  let entrypoints = Option.map (List.map (fun (e, t) ->
      e, Tzfunc_ppx.extract ~loc t, Tzfunc_ppx.produce ~loc t, Tzfunc_ppx.otype ~loc t)) options.entrypoints in
  let views = Option.map (List.map (fun (v, it, ot) ->
      v, Tzfunc_ppx.extract ~loc ot, Tzfunc_ppx.produce ~loc it, Tzfunc_ppx.otype ~loc it, Tzfunc_ppx.otype ~loc ot)) options.views in
  let (extract_storage_name, produce_storage_name, storage_type_name),
      entrypoints, views, bigmaps0 =
    match options.storage, entrypoints, views, options.bigmaps with
    | Some _, (None | Some []), (None | Some []), (None | Some []) ->
      ("extract", "produce", "t"), [], [], []
    | None, Some [n, e, p, t], (None | Some []), (None | Some []) ->
      ("", "", ""), [n, "", e, p, t], [], []
    | None, (None | Some []), Some [n, e, p, it, ot], (None | Some []) ->
      ("", "", ""), [], [n, "", e, p, it, ot], []
    | None, (None | Some []), (None | Some []), Some [_n, k, v] ->
      ("", "", ""), [], [], [ "", k, v ]
    | _ ->
      ("extract_storage", "produce_storage", "storage"),
      List.map (fun (n, e, p, t) -> (n, n, e, p, t)) @@ Option.value ~default:[] entrypoints,
      List.map (fun (n, e, p, it, ot) -> (n, n, e, p, it, ot)) @@ Option.value ~default:[] views,
      Option.value ~default:[] options.bigmaps
  in
  let views = List.map (fun (v, s, ex, pr, it, ot) ->
      if List.exists (fun (e, _, _, _, _) -> e = v) entrypoints then v, "view_" ^ s, ex, pr, it, ot
      else v, s, ex, pr, it, ot) views in
  let storage = storage ~loc ~name:extract_storage_name ?item options in
  let deploy = deploy ~loc ~name:produce_storage_name ?item options in
  let calls = List.filter_map (call ~loc ?item options) entrypoints in
  let view_calls = List.filter_map (view ~loc ?item options) views in
  let bigmaps = List.flatten @@ List.map (fun (n, tk, tv) -> [
        (if n = "" then "extract_key" else "extract_" ^ n ^ "_key"), Option.map (fun (tk, _) -> Tzfunc_ppx.extract ~loc tk) tk;
        (if n = "" then "extract_value" else "extract_" ^ n ^ "_value"), Option.map (Tzfunc_ppx.extract ~loc) tv;
        (if n = "" then "produce_key" else "produce_" ^ n ^ "_key"), Option.map (fun (tk, _) -> Tzfunc_ppx.produce ~loc tk) tk;
        (if n = "" then "produce_value" else "produce_" ^ n ^ "_value"), Option.map (Tzfunc_ppx.produce ~loc) tv;
        (if n = "" then "get" else "get_" ^ n), (match tk, tv with Some (_, typ), Some _ -> Some (get_bigmap ~loc ?item n typ) | _ -> None);
      ]) bigmaps0 in
  let bigmap_types = List.flatten @@ List.map (fun (n, tk, tv) -> [
        (if n = "" then "key" else n ^ "_key"), Option.map (fun (tk, _) -> Tzfunc_ppx.otype ~loc tk) tk;
        (if n = "" then "value" else n ^ "_value"), Option.map (Tzfunc_ppx.otype ~loc) tv;
      ]) bigmaps0 in
  let exprs = List.filter_map (function _, None -> None | txt, Some e -> Some (txt, e)) @@
    (extract_storage_name, extract_storage) :: (produce_storage_name, produce_storage) ::
    ("storage", storage) :: ("deploy", deploy) ::
    (List.flatten @@ List.map (fun (_, e, ex, pr, _) ->
         [(if e = "" then "extract" else "extract_" ^ e), Some ex;
          (if e = "" then "produce" else "produce_" ^ e), Some pr]) entrypoints) @
    (List.flatten @@ List.map (fun (_, e, ex, pr, _, _) ->
         [(if e = "" then "extract" else "extract_" ^ e), Some ex;
          (if e = "" then "produce" else "produce_" ^ e), Some pr]) views) @
    (List.map  (fun (e, c) -> e, Some c) calls) @
    (List.map  (fun (e, v) -> e, Some v) view_calls) @
    bigmaps @ [ "code", Option.map (estring ~loc) options.code ] in
  options, exprs, storage_type_name, storage_type,
  entrypoints, views, bigmap_types

let process_expr ~loc e =
  let options, exprs, _, _, _, _, _ = process ~loc e in
  let e =
    pexp_object ~loc @@ class_structure ~self:[%pat? self]
      ~fields:(List.map (fun (txt, e) ->
          pcf_method ~loc ({txt; loc}, Public, Cfk_concrete (Fresh, e))) exprs) in
  if options.debug then Format.printf "%s@." (Pprintast.string_of_expression e);
  e

let encoding ~loc t =
  let open Ppx_deriving_encoding_lib in
  Utils.wrap (Some "Proto.Encoding");
  let { Encoding.enc; _ } = Encoding.expressions ~wrap:true t in
  let enc_name = Utils.enc_name ~search:false t.ptype_name.txt in
  let typ =
    ptyp_constr ~loc (Utils.llid ~loc @@ Utils.enc_mod "encoding") [
      ptyp_constr ~loc (Utils.llid ~loc t.ptype_name.txt) [] ] in
  Utils.unwrap ();
  value_binding ~loc ~pat:(ppat_constraint ~loc (pvar ~loc enc_name) typ)
    ~expr:enc

let process_item ~loc e =
  let options, exprs, storage_type_name, storage_type, entrypoints, views, bigmap_types =
    process ~loc ~item:true e in
  let types, encodings = List.split @@ List.filter_map (function
      | _, None -> None
      | txt, Some t ->
        let td = type_declaration ~loc ~name:{txt; loc} ~params:[] ~cstrs:[]
            ~kind:Ptype_abstract ~private_:Public ~manifest:(Some t) in
        Some (
          pstr_type ~loc Recursive [td],
          pstr_value ~loc Nonrecursive [encoding ~loc td])
    ) @@
    (storage_type_name, storage_type) ::
    List.map (fun (_, e, _, _, t) -> (if e = "" then "t" else e), Some t) entrypoints @
    (List.flatten @@ List.map (fun (_, v, _, _, it, ot) -> [
        (if v = "" then "input" else v ^ "_input"), Some it;
        (if v = "" then "output" else v ^ "_output"), Some ot
      ]) views) @
    bigmap_types in
  let vals =
    List.map (fun (txt, expr) ->
        let vb = value_binding ~loc ~pat:(pvar ~loc txt) ~expr in
        pstr_value ~loc Nonrecursive [ vb ]) exprs in
  let str = if options.encodings then types @ encodings @ vals else types @ vals in
  if options.debug then Format.printf "%s@." (Pprintast.string_of_structure str);
  str

let record_to_object ~loc ~self e = match e.pexp_desc with
  | Pexp_record (l, None) ->
    pexp_object ~loc @@ class_structure ~self:(ppat_any ~loc)
      ~fields:(List.map (fun (lid, e) ->
          pcf_method ~loc ({lid with txt = Longident.name lid.txt}, Public,
                           Cfk_concrete (Fresh, self#expression e))) l)
  | _ -> e

let transform =
  object(self)
    inherit Ast_traverse.map as super
    method! expression e = match e.pexp_desc with
      | Pexp_apply ({pexp_desc=Pexp_ident {txt=Lident "!#"; _}; _}, [ Nolabel, e ])
      | Pexp_extension ({txt="tzfunc"; _}, PStr [ {pstr_desc=Pstr_eval (e, _); _} ]) ->
        dir_path := Filename.concat (Sys.getenv "PWD") (Filename.dirname e.pexp_loc.loc_start.pos_fname);
        Ast_helper.with_default_loc e.pexp_loc @@ fun () ->
        process_expr ~loc:e.pexp_loc e
      | Pexp_apply ({pexp_desc=Pexp_ident {txt=Lident "~#"; _}; _}, [ Nolabel, e ]) ->
        Ast_helper.with_default_loc e.pexp_loc @@ fun () ->
        record_to_object ~loc:e.pexp_loc ~self e
      | _ -> super#expression e

    method! structure_item st = match st.pstr_desc with
      | Pstr_value (Nonrecursive, [ {
          pvb_pat = {ppat_desc = Ppat_var {txt; _}; _};
          pvb_expr = {
            pexp_desc=Pexp_apply ({
                pexp_desc=Pexp_ident {txt=Lident "!#"; _}; _}, [ Nolabel, e ]); _}; _}
        ])
      | Pstr_value (Nonrecursive, [ {
          pvb_pat = {ppat_desc = Ppat_var {txt; _}; _};
          pvb_expr = {
            pexp_desc=Pexp_extension ({txt="tzfunc"; _}, PStr [ {pstr_desc=Pstr_eval (e, _); _} ]); _}; _}
          ]) ->
        dir_path := Filename.concat (Sys.getenv "PWD") (Filename.dirname e.pexp_loc.loc_start.pos_fname);
        let loc = e.pexp_loc in
        Ast_helper.with_default_loc loc @@ fun () ->
        let st = process_item ~loc e in
        pstr_module ~loc @@ module_binding ~loc ~name:{txt=Some (String.capitalize_ascii txt);loc}
          ~expr:(pmod_structure ~loc st)
      | _ -> super#structure_item st
  end

let () =
  Driver.register_transformation "tzfunc_ppx" ~impl:transform#structure
