# Tzfunc

Tzfunc is a minmal Tezos library.
The documentation can be found [here](https://functori.gitlab.io/dev/tzfunc/tzfunc/index.html).

## Requirements


## Install OPAM (OCaml Package Manager)

Follow the online [instructions](https://opam.ocaml.org/doc/Install.html) to install opam.

Initialize  your local switch, using opam:
```bash
opam switch create <switch_name> 4.12.0
```

## How to install TzFunc

    $ make build-deps
    $ make build
    $ make install

## License
Copyright © 2021, Functori <contact@functori.com>. Released under the [MIT License](https://gitlab.com/functori/tzfunc/-/blob/master/LICENSE).
